@extends('layouts.app')

@section('title')
    Alerts
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('css/mystyle.css')}}">
@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h4 class="card-title">Vos alertes</h4>
                                <a href="{{route('readAllNotifs')}}" style="text-decoration: none">Tous marquer comme lu</a>
                                {{--<button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuSizeButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Dropdown
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3">
                                    <h6 class="dropdown-header">Settings</h6>
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>--}}
                            </div>
                            <p class="card-description"></p>

                            @foreach($allnotif as $notif)
                                <div id="notifs" class="list d-flex align-items-center border-bottom py-3 espaceme {{$notif->read_at == '0' ? 'pas_vue' : ''}}">
                                    <img class="img-sm rounded-circle" src="{{url('piges/panneau/'.$notif->panneau)}}" alt="Photo du panneau">
                                    @if($notif->typenotif==null)
                                        <div class="wrapper w-100 ml-3">
                                            <p class="mb-0">
                                                <b>{{$notif->marque}} </b> <small>agence {{$notif->agence}}</small></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex align-items-center">
                                                    <a id="showNotif" href="{{route('profil_path',$notif->slug)}}" class="label label-primary" style="text-decoration: none">
                                                        <i class="fa fa-eye"></i> voir</a> {{--<input id="see" type="hidden" value="{{$notif->idnotif}}">--}}
                                                </div>
                                                <small class="text-muted ml-auto">{{date("d-m-Y H:i",strtotime($notif->dateM))}}</small>
                                            </div>
                                        </div>
                                    @else
                                        <div class="wrapper w-100 ml-3">
                                            <p class="mb-0">
                                                <b>{{$notif->marque}} </b> <small>agence {{$notif->agence}}</small></p>
                                            <p class="mb-0">
                                                <span class="text-danger">Pin rejeté</span>
                                            </p>
                                            <p class="mb-0">
                                                <i>{{nl2br($notif->datanotif)}}</i></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex align-items-center">
                                                    <a id="showNotif" href="{{route('profil_path',$notif->slug)}}" class="label label-primary" style="text-decoration: none">
                                                        <i class="fa fa-eye"></i> voir</a>
                                                </div>
                                                <small class="text-muted ml-auto">{{date("d-m-Y H:i",strtotime($notif->dateM))}}</small>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')

    </div>

@endsection



@section('footer_scripts')
    {{--<script type="text/javascript">
        $(document).ready(function () {
            $("#showNotif").click(function () {
                var id = $("#see").val();
                $.ajax
                ({
                    type: "get",
                    url:'{{ route("readNotif") }}',
                    timeout: 10000,
                    data:  'id=' + id,
                    beforeSend: function() {
                        //$('#loader').show();
                    },
                    success: function(response) {
                        var rep = JSON.parse(response);

                        if(rep.type == 1){
                            $('#notifs').removeClass('pas_vue');

                        }else{
                            console.log("error");
                        }
                    }
                });
            });
        });
    </script>--}}
@endsection
