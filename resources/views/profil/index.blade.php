@extends('layouts.app')

@section('title')
    Profil
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row profile-page">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="profile-header text-white d-none d-md-block">
                                <div class="d-flex justify-content-around">
                                    <div class="profile-info d-flex justify-content-center align-items-md-center">
                                        <img class="rounded-circle img-lg" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}" alt="image de profil">
                                        <div class="wrapper pl-4">
                                            <p class="profile-user-name">{{Auth::user()->name}} {{Auth::user()->prenoms}}</p>
                                            <div class="wrapper d-flex align-items-center">
                                                {{--<p class="profile-user-designation">User Experience Specialist</p>--}}
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="details">
                                        <div class="detail-col ">
                                            <p>Projects</p>
                                            <p>130</p>
                                        </div>
                                        <div class="detail-col ">
                                            <p>Projects</p>
                                            <p>130</p>
                                        </div>
                                    </div>--}}
                                </div>
                            </div>
                            <div class="profile-body pt-0 pt-sm-4">
                                {{--<ul class="nav tab-switch " role="tablist">
                                    <li class="nav-item ">
                                        <a class="nav-link active " id="user-profile-info-tab " data-toggle="pill " href="#user-profile-info" role="tab " aria-controls="user-profile-info " aria-selected="true ">Profile</a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link " id="user-profile-activity-tab " data-toggle="pill " href="#user-profile-activity" role="tab " aria-controls="user-profile-activity " aria-selected="false ">Activity</a>
                                    </li>
                                </ul>--}}
                                <ul class="nav tab-switch" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#whoweare" role="tab" aria-controls="whoweare" aria-selected="true">Profil</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ourgoal" role="tab" aria-controls="ourgoal" aria-selected="false">Modifier mon profil</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">Modifier mon mot de passe</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar" aria-selected="false">Modifier mon avatar</a>
                                    </li>
                                    @if(Auth::user()->typecompte==2)
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#portefeuille" role="tab" aria-controls="portefeuille" aria-selected="false">Mon portefeuille</a>
                                    </li>
                                    {{--<li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#collab" role="tab" aria-controls="collab" aria-selected="false">Mes collaborateurs</a>
                                    </li>--}}
                                    @endif
                                </ul>

                                <div class="tab-content tab-content-basic">
                                    <div class="tab-pane fade show active" id="whoweare" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="table-responsive ">
                                            <table class="table table-borderless w-100 mt-4 ">
                                                <tr>
                                                    <td>
                                                        <strong>Nom :</strong> {{Auth::user()->name}}</td>
                                                    <td>
                                                        <strong>Prénoms :</strong> {!! Auth::user()->prenoms ? Auth::user()->prenoms : '<span class="card-description">Non specifier</span>' !!}</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Adresse :</strong> {!! Auth::user()->address ? Auth::user()->address : '<span class="card-description">Non specifier</span>' !!}</td>
                                                    <td>
                                                        <strong>Email :</strong> {{Auth::user()->email}}</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Sexe :</strong> {{Auth::user()->sexe}}</td>
                                                    <td>
                                                        <strong>Contact :</strong> {{Auth::user()->contact}}</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Bio :</strong>
                                                        <textarea class="form-control" name="" id="" rows="5">{{Auth::user()->bio}}</textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ourgoal" role="tabpanel" aria-labelledby="profile-tab">
                                        <div style="padding: 2%">
                                            <form class="form-sample" method="post" action="{{route('updateuser')}}">
                                                {{csrf_field()}}
                                                <p class="card-description">
                                                    Information Personnel
                                                </p>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 col-form-label">Nom</label>
                                                            <div for="nom" class="col-sm-9">
                                                                <input type="text" value="{{Auth::user()->name}}" id="nom" name="nom" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="prenoms" class="col-sm-3 col-form-label">Prénoms</label>
                                                            <div class="col-sm-9">
                                                                <input value="{{Auth::user()->prenoms}}" id="prenoms" name="prenoms" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="sexe" class="col-sm-3 col-form-label">Sexe</label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="sexe" id="sexe" required>
                                                                    <option {{Auth::user()->sexe=='Homme' ? 'selected' : ''}} value="Homme">Homme</option>
                                                                    <option {{Auth::user()->sexe=='Femme' ? 'selected' : ''}} value="Femme">Femme</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="phone" class="col-sm-3 col-form-label">Contact</label>
                                                            <div class="col-sm-9">
                                                                <input value="{{Auth::user()->contact}}" id="phone" name="phone" class="form-control" placeholder="Contact">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if(Auth::user()->typecompte == '2')
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="phone" class="col-sm-3 col-form-label">Numéro
                                                                <br><small class="text-muted">Orange Money</small></label>
                                                            <div class="col-sm-9">
                                                                <input value="{{Auth::user()->numorange}}" id="phonemoney" name="phonemoney" class="form-control" placeholder="Numéro Orange Money" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif


                                                <br>
                                                <p class="card-description">
                                                    Information Géographique
                                                </p>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="address" class="col-sm-3 col-form-label">Addresse</label>
                                                            <div class="col-sm-9">
                                                                <input value="{{Auth::user()->address}}" id="address" name="address" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="bio" class="col-sm-3 col-form-label">Description</label>
                                                            <div class="col-sm-9">
                                                                <textarea class="form-control" name="bio" id="bio" rows="5">{{Auth::user()->bio}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                                <button type="submit" class="btn btn-success mr-2"><i class="fa fa-save"></i> Enregistrer</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="contact-tab">
                                        <div style="padding: 2%">
                                            <form class="forms-sample" method="post" action="{{route('updateuserpass')}}">
                                                {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="motpass">Mot de passe courant</label>
                                                    <input type="password" class="form-control" id="motpass" name="motpass" placeholder="Mot de passe courant">
                                                </div>
                                                <div class="form-group">
                                                    <label for="newpass">Nouveau mot de passe</label>
                                                    <input type="password" class="form-control" id="newpass" name="newpass" placeholder="Nouveau mot de passe">
                                                </div>
                                                <div class="form-group">
                                                    <label for="confirmpass">Confirmer le mot de passe</label>
                                                    <input type="password" class="form-control" id="confirmpass" name="confirmpass" placeholder="Confirmer le mot de passe">
                                                </div>
                                                <button type="submit" class="btn btn-success mr-2"><i class="fa fa-save"></i> Enregistrer</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                                        <div style="padding: 2%">
                                            <form class="clearfix" action="{{route('updateuseravatar')}}" method="POST" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-4">
                                                            <div class="thumbnail">
                                                                <img id="output" class="img-responsive" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}" alt="Image de profil" width="300" height="350" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-sm-1"></div>
                                                        <div class="col-md-8 col-sm-7">
                                                            <div class="sky-form nomargin">
                                                                <label id="file">Selectionner une image</label>
                                                                <input type="file" accept='image/*' id="file" name="fileUser" onchange="loadFile(event)" class="file-upload-default">
                                                                <div class="input-group col-xs-12">
                                                                    <input type="text" class="form-control file-upload-info" disabled placeholder="Selectionner une image">
                                                                    <span class="input-group-append">
                                                                      <button class="file-upload-browse btn btn-info" type="button">Télécharger</button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="margiv-top10">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                                                    <button type="submit" name="imgProfil" class="btn btn-danger"><i class="fa fa-check"></i> Enregistrer </button>
                                                    <button type="reset" class="btn btn-default">Annuler </button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="portefeuille" role="tabpanel" aria-labelledby="portefeuille-tab">
                                        <div style="padding: 2%">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <br>
                                                    <ul class="bullet-line-list">
                                                        <li>
                                                            @php $today = \Carbon\Carbon::today();
                                                                $begin = $today->startOfMonth()->format('d/m/y');
                                                                $end = $today->endOfMonth()->format('d/m/y');
                                                            @endphp
                                                            <h6>Nombre de panneau validé mise à jour <br><small class="text-muted">des trentes derniers jours ({{$begin}} - {{$end}})</small></h6>
                                                            <p class="mb-2"><span class="text-success">{{$countgains}} <i class="fa fa-map-marker"></i></span></p>
                                                        </li>
                                                        <li>
                                                            <h6>Montant<br><small class="text-muted">en Fcfa</small></h6>
                                                            <p class="mb-2"><span class="text-danger">{{ number_format($countgains* 100,2) }} Fcfa </span></p>
                                                        </li>
                                                        {{--<li>
                                                            <h6>Nombre de panneau validé<br><small class="text-muted">des quinze dernier jours</small></h6>
                                                            <p class="mb-2"><span class="text-warning">28 <i class="fa fa-map-marker"></i></span></p>
                                                        </li>--}}
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    <br>
                                                    <ul class="bullet-line-list">
                                                        <li>
                                                            <h6>Numéro<br><small class="text-muted">Orange Money</small></h6>
                                                            <p class="mb-2">{{Auth::user()->numorange}}</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--@if(Auth::user()->typecompte==2)
                                    <div class="tab-pane fade" id="collab" role="tabpanel" aria-labelledby="collab-tab">
                                        <div style="padding: 2%">
                                            <div class="row mb-4">
                                                <div class="col-12 d-flex align-items-center justify-content-between">
                                                    <h4 class="page-title"> </h4>
                                                    <div class="d-flex align-items-center">
                                                        <div class="wrapper">
                                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal-4"><i class="fa fa-user"></i> Ajouter un collaborateur</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <table id="order-listing" class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Ref.</th>
                                                        <th>Date d'adhésion</th>
                                                        <th>Nom&Prénom</th>
                                                        <th>Etat</th>
                                                    </tr>
                                                    </thead>
                                                    @if(!empty($fils))
                                                        <tbody>
                                                        @foreach($fils as  $k =>$user)
                                                            <tr>
                                                                <td>{{$k+1}}</td>
                                                                <td>{{$user->created_at->format('d-m-Y')}}</td>
                                                                <td>{{$user->name}} {{$user->prenoms}}</td>
                                                                <td>
                                                                    @if($user->etat==1)
                                                                        <label class="badge badge-success">Actif</label>
                                                                    @else
                                                                        <label class="badge badge-danger">Inactif</label>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @endif--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel-4">Nouveau collaborateur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form class="forms-sample" action="{{route('user-pigiste')}}" method="POST" autocomplete="off">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group divautre">
                                        <label for="nom">Nom</label>
                                        <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom">
                                    </div>
                                    <div class="form-group divautre">
                                        <label for="prenoms">Prénoms</label>
                                        <input type="text" class="form-control" name="prenoms" id="prenoms" placeholder="Prénoms">
                                    </div>
                                    <div class="form-group divautre">
                                        <label for="sexe">Sexe</label>
                                        <select class="form-control" name="sexe" id="sexe">
                                            <option value="Homme">Homme</option>
                                            <option value="Femme">Femme</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required data-inputmask="'alias': 'email'">
                                    </div>
                                    <div class="form-group">
                                        <label for="pass">Mot de passe</label>
                                        <input type="password" class="form-control" name="pass" id="pass" placeholder="Mot de passe" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="passconfirme">Confirmer le mot de passe</label>
                                        <input type="password" class="form-control" name="passconfirme" id="passconfirme" placeholder="Confirmer le mot de passe" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Ajouté</button>
                            <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')

    </div>

@endsection



@section('footer_scripts')
    <script src="{{ asset('js/form-addons.js')}} "></script>
    <script src="{{ asset('js/x-editable.js')}} "></script>
    <script src="{{ asset('js/file-upload.js')}} "></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script type="text/javascript">
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endsection
