<!--AFFICHER UN SET_FLASH DE SUCCESS-->
@if(Session::has('success'))
    <div class="alert alert-fill-success" role="alert">
        <i class="mdi mdi-alert-circle"></i>
        {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="margin-right: 10px;">&times;</span></button>
    </div>
@endif
<!--AFFICHER UN SET_FLASH DE SUCCESS-->

<!--AFFICHER UN SET_FLASH DE ERRORS-->
@if(Session::has('error'))
    <div class="alert alert-fill-danger" role="alert">
        <i class="mdi mdi-alert-circle"></i>
        {{Session::get('error')}}
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="margin-right: 10px;">&times;</span></button>
    </div>
@endif
<!--AFFICHER UN SET_FLASH DE ERROS-->

@if($errors->any())
    <div class="alert alert-fill-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="margin-right: 10px">&times;</span></button>
        @foreach($errors->all() as $errorr)
            {{ $errorr }}<br/>
        @endforeach
    </div>
@endif