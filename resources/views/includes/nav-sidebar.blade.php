<nav class="sidebar sidebar-offcanvas sidebar-dark" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <img src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}" alt="image de profil">
            <p class="text-center font-weight-medium">{{Auth::user()->name}}</p>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('home')}}">
                <i class="menu-icon menu-icon fa fa-home"></i>
                <span class="menu-title">Accueil</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('pins_path')}}">
                <i class="menu-icon menu-icon fa fa-map-marker"></i>
                <span class="menu-title">Pins</span>
            </a>
        </li>

        @if(Auth::user()->typecompte == '2')
            <li class="nav-item">
                <a class="nav-link" href="{{route('pins_add')}}">
                    <i class="menu-icon menu-icon fa fa-map-marker"></i>
                    <span class="menu-title">Ajouter Pins</span>
                </a>
            </li>
        @endif

        <li class="nav-item">
            <a class="nav-link" href="{{route('profiluser')}}">

                <i class="menu-icon menu-icon fa fa-user"></i>
                <span class="menu-title">Mon Compte</span>
            </a>
        </li>

        @if(Auth::user()->typecompte == '4')
        <li class="nav-item">
            <a class="nav-link" href="{{route('pins_path_valid')}}">
                <i class="menu-icon menu-icon fa fa-map-marker"><span class="text-warning"><i class="fa fa-check"></i></span></i>
                <span class="menu-title">Pins à valider</span>
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="{{route('user')}}">
                <i class="menu-icon menu-icon fa fa-users"></i>
                <span class="menu-title">Utilisateurs</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('annonceur_path')}}">
                <i class="menu-icon menu-icon fa fa-drivers-license-o"></i>
                <span class="menu-title">Annonceur</span>
            </a>
        </li>
        @endif

        @if(Auth::user()->typecompte == '3')
            <li class="nav-item">
                <a class="nav-link" href="{{route('pins_path_valid')}}">
                    <i class="menu-icon menu-icon fa fa-map-marker"><span class="text-warning"><i class="fa fa-check"></i></span></i>
                    <span class="menu-title">Pins à valider</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('user')}}">
                    <i class="menu-icon menu-icon fa fa-users"></i>
                    <span class="menu-title">Utilisateurs</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('annonceur_path')}}">
                    <i class="menu-icon menu-icon fa fa-drivers-license-o"></i>
                    <span class="menu-title">Annonceur</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('size_path')}}">
                    <i class="menu-icon menu-icon fa fa-signal"></i>
                    <span class="menu-title">Size</span>
                </a>
            </li>

            {{--<li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="menu-icon menu-icon fa fa-bars"></i>
                    <span class="menu-title">Catégorie</span>
                </a>
            </li>--}}

            <li class="nav-item">
                <a class="nav-link" href="{{route('alerts')}}">
                    <i class="menu-icon menu-icon mdi mdi-bell-outline"></i>
                    <span class="menu-title">Alertes</span>
                </a>
            </li>
        @endif

        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <i class="menu-icon menu-icon fa fa-power-off"></i>
                <span class="menu-title">Deconnexion</span>
            </a>
        </li>
    </ul>
</nav>