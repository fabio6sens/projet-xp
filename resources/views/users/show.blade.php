@extends('layouts.app')

@section('title')
    Information d'un utilisateur
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')

            <div class="row">
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card text-center">
                        <div class="card-body">
                            <img src="{{ $user->img  ? url('userAvatar/'.$user->img.'') : url('images/user.jpg') }}" class="img-lg rounded-circle mb-2" alt="profile image">
                            <h4>{{$user->name}} {{$user->prenoms}}</h4>
                            <p class="text-muted">@if($user->typecompte==1) Annonceur @elseif($user->typecompte==2) Pigiste @else Administrateur @endif</p>
                            <p class="mt-4 card-text">
                                {{Auth::user()->bio ? Auth::user()->bio : 'Sa description'}}
                            </p>
                            <div class="border-top pt-3">
                                {{--<div class="row">
                                    <div class="col-12">
                                        <h6>5896</h6>
                                        <p>Piges Suivie</p>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Information</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <br>
                                    <ul class="bullet-line-list">
                                        <li>
                                            <h6>Email</h6>
                                            <p class="mb-2">{{$user->email}} </p>
                                        </li>
                                        <li>
                                            <h6>Sexe</h6>
                                            <p class="mb-2">{{$user->sexe}} </p>
                                        </li>
                                        <li>
                                            <h6>Contact</h6>
                                            <p class="mb-2">{{$user->contact}} </p>
                                        </li>
                                        <li>
                                            <h6>Date d'adhésion</h6>
                                            <p class="mb-2">{{$user->created_at->format('d-m-Y')}} </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <br>
                                    <ul class="bullet-line-list">
                                        <li>
                                            <h6>Status</h6>
                                            <p class="mb-2">@if($user->etat==1) <label class="badge badge-success">Actif</label> @else <label class="badge badge-danger">Inactif</label> @endif </p>
                                        </li>
                                    @if($user->typecompte == '2')
                                        <li>
                                            <h6>Numéro Orange Money</h6>
                                            <p class="mb-2">{{$user->numorange}}</p>
                                        </li>
                                        @php $today = \Carbon\Carbon::today(); $begin = $today->startOfMonth()->format('d/m/y'); $end = $today->endOfMonth()->format('d/m/y');@endphp
                                        <li>
                                            <h6>Nombre de panneau validé mise à jour <br><small class="text-muted">des trentes derniers jours ({{$begin}} - {{$end}})</small></h6>
                                            <p class="mb-2"><span class="text-success">{{$countgains}} <i class="fa fa-map-marker"></i></span></p>
                                        </li>
                                        <li>
                                            <h6>Montant<br><small class="text-muted">en Fcfa</small></h6>
                                            <p class="mb-2"><span class="text-danger">{{ number_format($countgains* 100,2) }} Fcfa </span></p>
                                        </li>
                                    @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
@endsection
