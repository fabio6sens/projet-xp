@extends('layouts.app')

@section('title')
    Modifié un utilisateur
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Edité un compte</h4>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Veuillez renseigner ces champs</h4>
                    <form class="form-sample" action="{{route('editutilisateur',$user->slug)}}" method="POST" autocomplete="off">
                        {{csrf_field()}}
                        <p class="card-description">
                            Information Personelle
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="nom" class="col-sm-3 col-form-label">{{$user->typecompte=='1' ? 'Nom de l\'entreprise' : 'Nom' }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{$user->name}}" class="form-control" name="nom" id="nom" placeholder="Nom" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="prenoms" class="col-sm-3 col-form-label">Prénoms</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{$user->prenoms}}" class="form-control" name="prenoms" id="prenoms" placeholder="Prénoms" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="sexe" class="col-sm-3 col-form-label">Sexe</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="sexe" id="sexe" required>
                                            <option {{$user->sexe=='Homme' ? 'selected' : ''}} value="Homme">Homme</option>
                                            <option {{$user->sexe=='Femme' ? 'selected' : ''}} value="Femme">Femme</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="typecompte">Type de compte</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="typecompte" id="typecompte" required>
                                            <option value="1" {{$user->typecompte=='1' ? 'selected' : ''}}>Annonceur</option>
                                            <option value="2" {{$user->typecompte=='2' ? 'selected' : ''}}>Pigiste</option>
                                            <option value="4" {{$user->typecompte=='4' ? 'selected' : ''}}>Administrateur</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="etat">Status</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="etat" id="etat" required>
                                            <option value="1" {{$user->etat=='1' ? 'selected' : ''}}>Actif</option>
                                            <option value="0" {{$user->etat=='0' ? 'selected' : ''}}>Inactif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Mise à jour</button>
                            <button type="reset" class="btn btn-light" data-dismiss="modal">Annuler</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
@endsection
