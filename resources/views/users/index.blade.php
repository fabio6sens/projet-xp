@extends('layouts.app')

@section('title')
    Utilisateurs
    @parent
@stop

@section('header_styles')
    <style>
        .hide{
            display: none;
        }
    </style>
@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Gestion des comptes</h4>
                    <div class="d-flex align-items-center">
                        <div class="wrapper">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal-4"><i class="fa fa-user"></i> Ajouter un compte</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4">Nouveau compte</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="forms-sample" action="{{route('user')}}" method="POST" autocomplete="off">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="display: flex">
                                            <div class="col-md-4">
                                                <label>Annonceur
                                                <input type="radio" name="typecompte" id="annonceur" value="1"></label>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Pigiste
                                                <input type="radio" name="typecompte" id="pigiste" value="2" checked></label>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Administrateur
                                                <input type="radio" name="typecompte" id="admin" value="4"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group hide divannonceur">
                                            <label for="compagny">Nom de l'Entreprise</label>
                                            <input type="text" class="form-control" name="compagny" id="compagny" placeholder="Nom de l'Entreprise">
                                        </div>
                                        <div class="form-group divautre">
                                            <label for="nom">Nom</label>
                                            <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom">
                                        </div>
                                        <div class="form-group divautre">
                                            <label for="prenoms">Prénoms</label>
                                            <input type="text" class="form-control" name="prenoms" id="prenoms" placeholder="Prénoms">
                                        </div>
                                        <div class="form-group divautre">
                                            <label for="sexe">Sexe</label>
                                            <select class="form-control" name="sexe" id="sexe">
                                                <option value="Homme">Homme</option>
                                                <option value="Femme">Femme</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required data-inputmask="'alias': 'email'">
                                        </div>
                                        <div class="form-group">
                                            <label for="pass">Mot de passe</label>
                                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Mot de passe" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="passconfirme">Confirmer le mot de passe</label>
                                            <input type="password" class="form-control" name="passconfirme" id="passconfirme" placeholder="Confirmer le mot de passe" required>
                                        </div>
                                        {{--<div class="form-group">
                                            <label for="typecompte">Type de compte</label>
                                            <select class="form-control" name="typecompte" id="typecompte" required>
                                                <option value="1">Annonceur</option>
                                                <option value="2">Pigiste</option>
                                                <option value="3">Administrateur</option>
                                            </select>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Ajouté</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Ref.No.#</th>
                                    <th>Date d'adhésion</th>
                                    <th>Nom&Prénoms</th>
                                    <th>Sexe</th>
                                    <th>Email</th>
                                    <th>Type Compte</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                @if(!empty($users))
                                    <tbody>
                                        @foreach($users as  $k =>$user)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>{{$user->created_at->format('d-m-Y')}}</td>
                                            <td>{{$user->name}} {{$user->prenoms}}</td>
                                            <td>{{$user->sexe}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                @if($user->typecompte==1)
                                                    <label class="badge badge-primary">Annonceur</label>
                                                @elseif($user->typecompte==2)
                                                    <label class="badge badge-info">Pigiste</label>
                                                @elseif($user->typecompte==4)
                                                    <label class="badge badge-dark">Administrateur</label>
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->etat==1)
                                                    <label class="badge badge-success">Actif</label>
                                                @else
                                                    <label class="badge badge-danger">Inactif</label>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('showutilisateur',$user->slug)}}" class="btn btn-outline-primary"><i class="mdi mdi-eye text-primary"></i> </a>
                                                <a href="{{route('edit-utilisateur',$user->slug)}}" class="btn btn-outline-success"><i class="fa fa-edit text-info"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>

    <script>
        $(document).ready(function () {
            $("input:radio").click(function () {
                if ($('input:radio#annonceur:checked').length > 0) {
                    $('.divannonceur').removeClass('hide');
                    $('.divautre').addClass('hide');
                } else {
                    $('.divautre').removeClass('hide');
                    $('.divannonceur').addClass('hide')
                }
            });
        });
    </script>
@endsection
