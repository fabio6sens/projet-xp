@extends('layouts.app')

@section('title')
    Nouveau annonceur
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title"><i class="fa fa-drivers-license-o"></i> Nouveau Annonceur</h4>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Veuillez renseigner ces champs</h4>
                    <br>
                    <form class="forms-sample" action="{{route('annonce_get_path_store')}}" method="POST" autocomplete="off">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="type">Nom</label>
                                        <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="iddoublon" id="iddoublon" value="{{$id}}">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><i class="fa fa-drivers-license-o"></i> Ajouté</button>
                            <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')

@endsection
