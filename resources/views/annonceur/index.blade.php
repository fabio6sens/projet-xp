@extends('layouts.app')

@section('title')
    Annonceur
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Gestion des annonceurs</h4>
                    <div class="d-flex align-items-center">
                        <div class="wrapper">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal-4"><i class="fa fa-drivers-license-o"></i> Ajouter un annonceur</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4"><i class="fa fa-drivers-license-o"></i> Nouveau annonceur</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="forms-sample" action="{{route('annonce_store_path')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="type">Nom</label>
                                            <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="etat">Etat</label>
                                            <select class="form-control" name="etat" id="etat" required>
                                                <option value="1">Actif</option>
                                                <option value="0">Inactif</option>
                                            </select>
                                        </div>

                                        {{--<div class="form-group">
                                            <label for="file">Marqueur </label>
                                            <input type="file" id="file" name="file" class="dropify" required/>
                                        </div>--}}

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-drivers-license-o"></i> Ajouté</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{--#################################### Version Update Modal #############################################--}}
            <div class="modal fade" id="versionUpdateModal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-sd">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4"><i class="fa fa-drivers-license-o"></i> Mise à jour</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="formsUpdate" action="{{route('annonce_update_path')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="type">Nom</label>
                                            <input type="text" class="form-control" name="libelle" id="libelle" placeholder="Nom" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="etat">Etat</label>
                                            <select class="form-control" name="etatUpdate" id="etatUpdate" required>
                                                <option value="1">Actif</option>
                                                <option value="0">Inactif</option>
                                            </select>
                                        </div>
                                        {{--<div class="row">
                                            <div class="col-md-6" id="markerImg">

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="file">Nouveau marqueur</label>
                                                    <input type="file" id="file" name="file" class="dropify"/>
                                                </div>
                                            </div>
                                        </div>--}}

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="slug" id="slug">
                                <button type="submit" id="updateBtn" class="btn btn-success" style="display: none"><i class="fa fa-drivers-license-o"></i> Modifié</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{--#################################### Version Update Modal #############################################--}}
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Ref.No.#</th>
                                    {{--<th>Marqueur</th>--}}
                                    <th>Nom</th>
                                    <th>Etat</th>
                                    <th>Date d'ajout</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @if(!empty($annonceurs))
                                    <tbody>
                                        @foreach($annonceurs as  $k =>$annonce)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            {{--<td class="py-1">--}}
                                                {{--<img src="{{ $annonce->marqueur  ? url('piges/marqueurs/'.$annonce->marqueur.'') : '' }}" alt="marqueur">--}}
                                            {{--</td>--}}
                                            <td>{{$annonce->libelle}}</td>
                                            <td>
                                                @if($annonce->etat==1)
                                                    <label class="badge badge-success">Actif</label>
                                                @else
                                                    <label class="badge badge-danger">Inactif</label>
                                                @endif
                                            </td>
                                            <td>{{$annonce->created_at->format('d-m-Y')}}</td>
                                            <td>
                                                <a href="#" class="btn btn-outline-success" aria-expanded="false" title="Modifier" id="modifier" data-toggle="modal" onclick="displayUpdate('{{$annonce->id}}')" data-target="#versionUpdateModal"><i class="fa fa-edit text-info"></i> Edité</a>
                                                {{--<a href="" class="btn btn-outline-success"><i class="fa fa-edit text-info"></i> Edité</a>--}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    {{--<script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>--}}
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/alerts.js')}} "></script>

    <script type="text/javascript">
        function displayUpdate(id) {
            $("#formsUpdate").empty();
            //$("#markerImg").empty();
            $("#slug").val(id);
            $.ajax
            ({
                type: "get",
                url: "{{ route('annonceur_info_path') }}",
                timeout: 10000,
                data: 'id=' + id ,
                success: function (response) {
                    $("#updateBtn").show();
                    var rep = JSON.parse(response);
                    //console.log(rep);
                    $("#libelle").val(rep.data[0].libelle);
                   /* var img='<img src="piges/marqueurs/'+rep.data[0].marqueur+'" alt="old marker">';
                    $("#markerImg").append(img);*/

                    var select = document.getElementById('etatUpdate');
                    var option;

                    for (var i=0; i<select.options.length; i++) {
                        option = select.options[i];
                        if (option.value == rep.data[0].etat) {
                            option.selected = true;
                            return;
                        }
                    }
                }
            });

        }
    </script>

@endsection
