@extends('layouts.app')

@section('title')
    Pin {{$pins->annonceurPins->libelle}}
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('vendors/lightgallery/css/lightgallery.css')}}">
@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h2>{{$pins->annonceurPins->libelle}}</h2>
                                    <h4><i class="fa fa-clock-o"></i> Dernière mise à jour <span class="text-success">{{$pins->updated_at->diffForHumans()}}</span></h4>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-6">
                                    <img class="img-fluid rounded" src="{{url('piges/panneau/'.$pins->panneau)}}" alt="Photo du panneau">
                                    <div id="lightgallery" class="lightGallery">
                                        <a href="{{url('piges/panneau/'.$pins->panneau)}}" class="Photo du panneau">
                                            <button draggable="false" title="Plein écran" aria-label="Plein écran" type="button" class="" style="background: none rgb(255, 255, 255);border: 0px;margin: 10px;padding: 0px;position: absolute;cursor: pointer;user-select: none;border-radius: 2px;height: 40px;width: 40px;box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;overflow: hidden;top: 0px;">
                                                <img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%20018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 11px;">
                                            </button>
                                        </a>
                                    </div>
                                    @if($double != null)
                                        <a href="{{route('profil_path',$double->slug)}}" class="btn btn-info btn-sm mt-3 mb-4"><i class="fa fa-eye"></i> Voir le second panneau</a>
                                    @endif
                                </div>
                                <div class="col-md-6 pl-md-5">
                                    <div class="col-md-12">
                                        <div id="mapprofil" style="width:100%; height:200px;"></div>
                                    </div>
                                    <br>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Type</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->type}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Taille</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!!$pins->taillePins->taille !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Etat</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->etat}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Secteur d'activité</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->sectActiv}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Marque</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->marque}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Agence</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->agence}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Achat HT</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!!$pins->achat ? $pins->achat : '<span class="text-danger">Pas renseigner</span>'!!}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Vente HT</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!!$pins->vente ? $pins->vente : '<span class="text-danger">Pas renseigner</span>'!!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">ODM</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!! $pins->odm ? $pins->odm : '<span class="text-danger">Pas renseigner</span>' !!}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">TM</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!!$pins->tm ? $pins->tm : '<span class="text-danger">Pas renseigner</span>'!!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Eclairage</h6>
                                                    <p class="mb-0 text-muted">
                                                        {!!$pins->eclaire ? $pins->eclaire : '<span class="text-danger">Pas renseigner</span>'!!}
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Campagne</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->campagne ? $pins->campagne : 'Pas renseigner'}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Régie Pub</h6>
                                                    <p class="mb-0 text-muted">
                                                        {{$pins->regie}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="media-body">
                                                    <h6 class="mb-1">Note</h6>
                                                    <p class="mb-0 text-muted">
                                                        <select id="example-fontawesome" name="rating" autocomplete="off">
                                                            <option value="1" {{$pins->note=='1' ? 'selected' : ''}}>1</option>
                                                            <option value="2" {{$pins->note=='2' ? 'selected' : ''}}>2</option>
                                                            <option value="3" {{$pins->note=='3' ? 'selected' : ''}}>3</option>
                                                            <option value="4" {{$pins->note=='4' ? 'selected' : ''}}>4</option>
                                                            <option value="5" {{$pins->note=='5' ? 'selected' : ''}}>5</option>
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <input type="hidden" id="lat" value="{{$pins->lat}}">
        <input type="hidden" id="lon" value="{{$pins->lng}}">
        @include('includes.footer')

    </div>

@endsection



@section('footer_scripts')
    <script src="{{ asset('js/tabs.js')}}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/alerts.js')}} "></script>
    <script src="{{ asset('vendors/lightgallery/js/lightgallery-all.min.js')}} "></script>
    <script src="{{ asset('js/light-gallery.js')}} "></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //initialize();
            google.maps.event.addDomListener(window, 'load', initialize);
        });
        function initialize() {
            var lat = $("#lat").val();
            var lon = $("#lon").val();
            var myLatlng = new google.maps.LatLng(lat, lon);

            var mapOptions = {
                zoom: 14,
                center: myLatlng,
                scrollwheel:true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById('mapprofil'), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation:  google.maps.Animation.DROP
            });
        }

        //google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection
