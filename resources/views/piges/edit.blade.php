@extends('layouts.app')

@section('title')
    Modifié un pin
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Edité un pin</h4>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Veuillez renseigner ces champs</h4>
                    <p class="card-description">Tous les champs marqués de <span class="text-danger">*</span> sont obligatoires</p>
                    <br>
                    <form class="form-sample" action="{{route('pins_update_path',$pin->slug)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="annonceur">Campagne <span class="text-danger">*</span></label>
                                    <input type="text" value="{{$pin->campagne}}" class="form-control" name="campagne" id="campagne" placeholder="Campagne" required>
                                </div>

                                <div class="form-group">
                                    <label for="type">Type <span class="text-danger">*</span></label>
                                    <select id="type" name="type" class="form-control">
                                        <option value="panneaux standard" {{$pin->type=='panneaux standard' ? 'selected' : ''}}>Panneaux standard</option>
                                        <option value="panneaux numeriques" {{$pin->type=='panneaux numeriques' ? 'selected' : ''}}>Panneaux numériques</option>
                                        <option value="panneaux double" {{$pin->type=='panneaux double' ? 'selected' : ''}}>Panneaux double</option>
                                        <option value="triples panneaux" {{$pin->type=='triples panneaux' ? 'selected' : ''}}>Triples panneaux</option>
                                        <option value="grands panneaux" {{$pin->type=='grands panneaux' ? 'selected' : ''}}>Grands panneaux</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="taille">Taille <span class="text-danger">*</span></label>
                                    <select class="form-control" name="taille" id="taille" required>
                                        @if(count($tailles) > 0)
                                            @foreach($tailles as $taille)
                                                <option value="{{$taille->id}}" {{$pin->taille_id==$taille->id ? 'selected' : ''}}>{!! $taille->taille !!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="type">Eclairage <span class="text-danger">*</span></label>
                                    <select class="form-control" id="eclaire" name="eclaire" autocomplete="off">
                                        <option value="oui" {{$pin->eclaire=='oui' ? 'selected' : ''}}>Oui</option>
                                        <option value="non" {{$pin->eclaire=='non' ? 'selected' : ''}}>Non</option>
                                    </select>
                                    {{--<input type="text" class="form-control" name="type" id="type" placeholder="Type" required>--}}
                                </div>
                                <div class="form-group">
                                    <label for="annonceur">Etat <span class="text-danger">*</span></label>
                                    <select class="form-control" id="etat" name="etat" autocomplete="off">
                                        <option value="PROPRE" {{$pin->etat=='PROPRE' ? 'selected' : ''}}>PROPRE</option>
                                        <option value="DEGRADE" {{$pin->etat=='DEGRADE' ? 'selected' : ''}}>DEGRADE</option>
                                        <option value="ENCOMBRE" {{$pin->etat=='ENCOMBRE' ? 'selected' : ''}}>ENCOMBRE</option>
                                    </select>
                                <!--<input value="{{$pin->etat}}" type="text" class="form-control" name="etat" id="etat" placeholder="Etat" required>-->
                                </div>
                                <div class="form-group">
                                    <label for="annonceur">Annonceur <span class="text-danger">*</span></label>
                                    <select name="annonceur" required id="annonceur" class="form-control">
                                        @if(count($annonceurs) > 0)
                                            @foreach($annonceurs as $annonce)
                                                <option value="{{$annonce->id}}" {{$pin->annonceur_id==$annonce->id ? 'selected' : ''}}>{{$annonce->libelle}}</option>
                                            @endforeach
                                                <option value="autre">AUTRE</option>
                                        @else
                                            <option value="0">Inconnu</option>
                                        @endif
                                    </select>
                                    {{--<input type="text" class="form-control" name="annonceur" id="annonceur" placeholder="Annonceur" >--}}
                                </div>
                                <div class="form-group">
                                    <label for="activ">Secteur d'activité <span class="text-danger">*</span></label>
                                    <input value="{{$pin->sectActiv}}" type="text" class="form-control" name="activ" id="activ" placeholder="Secteur d'activité" required>
                                </div>
                                <div class="form-group">
                                    <label for="marque">Marque <span class="text-danger">*</span></label>
                                    <input value="{{$pin->marque}}" type="text" class="form-control" name="marque" id="marque" placeholder="Marque" required>
                                </div>
                                <div class="form-group">
                                    <label for="agence">Agence Pub <span class="text-danger">*</span></label>
                                    <input value="{{$pin->agence}}" type="text" class="form-control" name="agence" id="agence" placeholder="Agence Publicitaire" required>
                                </div>
                                <div class="form-group">
                                    <label for="regie">Régie Pub <span class="text-danger">*</span></label>
                                    <input value="{{$pin->regie}}" type="text" class="form-control" name="regie" id="regie" placeholder="Régie Publicitaire" required>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="achat">Achat HT</label>
                                            <input value="{{$pin->achat}}" type="number" class="form-control" name="achat" id="achat" placeholder="Achat HT">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="vente">Vente HT</label>
                                            <input value="{{$pin->vente}}" type="number" class="form-control" name="vente" id="vente" placeholder="Vente HT">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="odm">ODM</label>
                                            <input value="{{$pin->odm}}" type="number" class="form-control" name="odm" id="odm" placeholder="Occupation du domaine public">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tm">TM</label>
                                            <input value="{{$pin->tm}}" type="number" class="form-control" name="tm" id="tm" placeholder="Taxes Municipales">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="note">Note</label>
                                    <div class="br-wrapper br-theme-fontawesome-stars">
                                        <select id="example-fontawesome" name="note" autocomplete="off" style="display: none;">
                                            <option value="1" {{$pin->note=='1' ? 'selected' : ''}}>1</option>
                                            <option value="2" {{$pin->note=='2' ? 'selected' : ''}}>2</option>
                                            <option value="3" {{$pin->note=='3' ? 'selected' : ''}}>3</option>
                                            <option value="4" {{$pin->note=='4' ? 'selected' : ''}}>4</option>
                                            <option value="5" {{$pin->note=='5' ? 'selected' : ''}}>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="mappins" style="height:200px;"></div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="lat">Latitude <span class="text-danger">*</span></label>
                                            <input value="{{$pin->lat}}" type="text" class="form-control" name="lat" id="lat" required readonly>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="lng">Longitude <span class="text-danger">*</span></label>
                                            <input value="{{$pin->lng}}" type="text" class="form-control" name="lng" id="lng" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file">Photo du panneau <span class="text-danger">*</span></label>
                                    <input type="file" id="file" name="file" class="dropify" data-default-file="{{url('piges/panneau/'.$pin->panneau)}}"/>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Mise à jour</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //initialize();
            google.maps.event.addDomListener(window, 'load', points);

            $("#annonceur").change(function () {
                if($("#annonceur").val()=='autre'){
                    console.log("vous allez etre redirige");
                    window.location='{{ route("annonceur_path") }}';
                }
            });

        });

        function points(){
            //console.log(position);
            lat = $("#lat").val();
            lon = $("#lng").val();


            var LatLng = new google.maps.LatLng(lat,lon);

            map = new google.maps.Map(document.getElementById('mappins'), {
                center: LatLng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
            });

            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                animation: google.maps.Animation.DROP,
                title: 'je suis ici'
            });
        }

        //google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection
