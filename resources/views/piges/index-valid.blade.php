@extends('layouts.app')

@section('title')
    Pins à valider
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Pin à valider</h4>
                </div>
            </div>

            <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4">Rejet d'un pin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form id="myforms-sample" class="forms-sample" action="{{route('pins_rejet')}}" method="post" autocomplete="off">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="motifrejet">Motif <span class="text-danger">*</span></label>
                                            <textarea class="form-control" name="motifrejet" id="motifrejet" rows="4" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="ipop" id="vipop">
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Rejeter</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Ref.</th>
                                    <th>Etat</th>
                                    <th>Update</th>
                                    <th>Type</th>
                                    <th>Taille</th>
                                    <th>Etat</th>
                                    <th>Annonceur</th>
                                    <th>Secteur activité</th>
                                    <th>Marque</th>
                                    <th>Agence</th>
                                    <th>Régie</th>
                                    <th>Pigiste</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @if(!empty($pins))
                                    <tbody>
                                    @foreach($pins as  $k =>$pin)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>
                                                @if($pin->actif==0)
                                                    <label class="badge badge-warning">Attente de validation</label>
                                                @elseif($pin->actif==2)
                                                    <label class="badge badge-success">Rejeter</label>
                                                @endif
                                            </td>
                                            <td>{{$pin->updated_at->diffForHumans()}}</td>
                                            <td>{{$pin->type}}</td>
                                            <td>{!!$pin->taillePins->taille !!}</td>
                                            <td>{{$pin->etat}}</td>
                                            <td>{{$pin->annonceurPins->libelle}}</td>
                                            <td>{{$pin->sectActiv}}</td>
                                            <td>{{$pin->marque}}</td>
                                            <td>{{$pin->agence}}</td>
                                            <td>{{$pin->regie}}</td>
                                            <td>{{$pin->userPins->name}} {{$pin->userPins->prenoms}}</td>
                                            <td>
                                                <a href="{{route('profil_path',$pin->slug)}}" class="btn btn-outline-primary"><i class="mdi mdi-eye text-primary"></i> </a>
                                                <a href="{{route('pins_valid',$pin->slug)}}" class="btn btn-outline-success"><i class="fa fa-check text-info"></i></a>
                                                <button onclick=motif('{{$pin->slug}}') type="button" data-toggle="modal" data-target="#exampleModal-4" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/alerts.js')}} "></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //initialize();
            document.body.className='sidebar-icon-only';
        });

        function motif(blakobla){
            $("#myforms-sample")[0].reset();
            $("#vipop").val(blakobla);
        }
    </script>
@endsection
