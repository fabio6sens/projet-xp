@extends('layouts.app')

@section('title')
    Nouveau pins
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title"><i class="fa fa-map-marker"></i> Nouveau pin</h4>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Veuillez renseigner ces champs</h4>
                    <p class="card-description">Tous les champs marqués de <span class="text-danger">*</span> sont obligatoires</p>
                    <br>
                    <form class="forms-sample" action="{{route('pins_store_path')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    {{--<div class="form-group">
                                        <label for="annonceur">Campagne <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="campagne" id="campagne" placeholder="Campagne" required>
                                    </div>--}}

                                    <div class="form-group">
                                        <label for="type">Type <span class="text-danger">*</span></label>
                                        <select class="form-control" id="type" name="type" autocomplete="off">
                                            <option value="panneaux standard">Panneaux standard</option>
                                            <option value="panneaux numeriques">Panneaux numériques</option>
                                            <option value="panneaux double">Panneaux double</option>
                                            <option value="triples panneaux">Triples panneaux</option>
                                            <option value="grands panneaux">Grands panneaux</option>
                                        </select>
                                        {{--<input type="text" class="form-control" name="type" id="type" placeholder="Type" required>--}}
                                    </div>
                                    <div class="form-group">
                                        <label for="taille">Taille <span class="text-danger">*</span></label>
                                        <select class="form-control" name="taille" id="taille" required>
                                            @if(count($tailles) > 0)
                                                @foreach($tailles as $taille)
                                                    <option value="{{$taille->id}}">{!! $taille->taille !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    {{--<div class="form-group">
                                        <label for="type">Eclairage <span class="text-danger">*</span></label>
                                        <select class="form-control" id="eclaire" name="eclaire" autocomplete="off">
                                            <option value="oui">Oui</option>
                                            <option value="non">Non</option>
                                        </select>
                                        --}}{{--<input type="text" class="form-control" name="type" id="type" placeholder="Type" required>--}}{{--
                                    </div>
                                    <div class="form-group">
                                        <label for="annonceur">Etat <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="etat" id="etat" placeholder="Etat" required>
                                    </div>--}}
                                    <div class="form-group">
                                        <label for="annonceur">Annonceur</label>
                                        <select name="annonceur" required id="annonceur" class="form-control">
                                            @if(count($annonceurs) > 0)
                                                @foreach($annonceurs as $annonce)
                                                    <option value="{{$annonce->id}}">{{$annonce->libelle}}</option>
                                                @endforeach
                                                    <option value="autre">AUTRE</option>
                                            @else
                                                <option value="0">Inconnu</option>
                                            @endif
                                        </select>
                                        {{--<input type="text" class="form-control" name="annonceur" id="annonceur" placeholder="Annonceur" >--}}
                                    </div>
                                    {{--<div class="form-group">
                                        <label for="activ">Secteur d'activité <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="activ" id="activ" placeholder="Secteur d'activité" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="marque">Marque <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="marque" id="marque" placeholder="Marque" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="agence">Agence Pub <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="agence" id="agence" placeholder="Agence Publicitaire" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="regie">Régie Pub <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="regie" id="regie" placeholder="Régie Publicitaire" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="achat">Achat HT</label>
                                                <input type="number" class="form-control" name="achat" id="achat" placeholder="Achat HT">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="vente">Vente HT</label>
                                                <input type="number" class="form-control" name="vente" id="vente" placeholder="Vente HT">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="odm">ODM</label>
                                                <input type="number" class="form-control" name="odm" id="odm" placeholder="Occupation du domaine public">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="tm">TM</label>
                                                <input type="number" class="form-control" name="tm" id="tm" placeholder="Taxes Municipales">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="note">Note <span class="text-danger">*</span></label>
                                        <div class="br-wrapper br-theme-fontawesome-stars">
                                            <select id="example-fontawesome" name="note" autocomplete="off" style="display: none;">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>--}}
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div id="mappins" style="height:200px;"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-9"></div>
                                            <div class="col-md-3">
                                                {{--<div id="spinner"><i class="fa fa-spin fa-spinner"></i></div>--}}
                                                <button type="button" id="genere" onclick="genererMap()" class="btn btn-danger btn-xs"><i class="fa fa-crosshairs" style="margin: 0px"></i></button>
                                                {{--<button type="button" id="shows" onclick="showLatLon()" class="btn btn-success btn-xs"><i class="fa fa-pencil" style="margin: 0px"></i></button>--}}
                                                <button type="button" id="mask" onclick="maskLatLon()" style="display: none" class="btn btn-light btn-xs"><i class="fa fa-eye-slash" style="margin: 0px"></i></button>
                                                <span style="margin-bottom: 3px"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="lat">Latitude <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="lat" id="lat" required readonly>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="lng">Longitude <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="lng" id="lng" required readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="file">Photo du panneau <span class="text-danger">*</span></label>
                                        <input type="file" id="file" name="file" class="dropify" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><i class="fa fa-map-marker"></i> Ajouté</button>
                            <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/alerts.js')}} "></script>
    <script src="{{ asset('js/select2.js')}} "></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //$("#spinner").hide();
            //initialize();
            //document.body.className='sidebar-icon-only';
            google.maps.event.addDomListener(window, 'load', geoLocationInit);

            $("#annonceur").change(function () {
                if($("#annonceur").val()=='autre'){
                    console.log("vous allez etre redirige");
                    window.location='{{ route("annonce_get_path",0) }}';
                }
            });

            if($("#lat").val()==''){
                console.log('null');
                $("#genere").show();
            }else{
                console.log('no null');
                $("#genere").hide();
            }

        });

        function showLatLon(){
            $("#lat").attr("readonly", false);
            $("#lng").attr("readonly", false);
            $("#shows").hide();
            $("#mask").show();
        }
        function maskLatLon(){
            $("#lat").attr("readonly", true);
            $("#lng").attr("readonly", true);
            $("#shows").show();
            $("#mask").hide();
        }

        function genererMap(){
            //$("#spinner").show();
            console.log("test");
            navigator.geolocation.getCurrentPosition(success,fail);
            /*if($("#lat").val()!=null){
                $("#spinner").hide();
            }*/
        }

        function geoLocationInit(){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success,fail);
            }else{
                swal({
                    title: 'Oups!',
                    text: 'Veuillez activer la localisation de votre navigateur',
                    icon: 'error',
                    button: {
                        text: "Continue",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"
                    }
                })
            }
        }

        function success(position){
            //console.log(position);
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            $("#lat").val(lat);
            $("#lng").val(lon);

            var LatLng = new google.maps.LatLng(lat,lon);

            map = new google.maps.Map(document.getElementById('mappins'), {
                center: LatLng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
            });

            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                animation:google.maps.Animation.BOUNCE,
                title: 'je suis ici'
            });
        }

        function fail(error){
            console.log(error);
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    //alert("Vous avez annuler la géolocalisation de cet document.");
                    swal({
                        title: 'Oups!',
                        text: 'Vous avez annuler votre géolocalisation',
                        icon: 'error',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    });
                    break;
                case error.POSITION_UNAVAILABLE:
                    //alert("Information de géolocalisation indéfini. Vérifié votre connexion internet");
                    swal({
                        title: 'Oups!',
                        text: 'Information de géolocalisation indéfini. Vérifié votre connexion internet',
                        icon: 'error',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    });
                    break;
                case error.TIMEOUT:
                    //alert("La periode d'attente est dépassé. Vérifié votre connexion internet");
                    swal({
                        title: 'Oups!',
                        text: 'La periode d\'attente est dépassé. Vérifié votre connexion internet',
                        icon: 'error',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    });
                    break;
                case error.UNKNOWN_ERROR:
                    //alert("Erreur inconnue. Vérifié votre connexion internet");
                    swal({
                        title: 'Oups!',
                        text: 'Erreur inconnue. Vérifié votre connexion internet',
                        icon: 'error',
                        button: {
                            text: "Continue",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    });
                    break;
            }
        }
        //google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection
