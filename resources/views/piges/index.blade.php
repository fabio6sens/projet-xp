@extends('layouts.app')

@section('title')
    Pins
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Gestion des pins</h4>
                    <div class="d-flex align-items-center">
                        <div class="wrapper">
                            <a type="button" class="btn btn-success" href="{{route('pins_add')}}"><i class="fa fa-map-marker"></i> Ajouter un pins</a>
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4"><i class="fa fa-map-marker"></i> Nouveau pins</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="forms-sample" action="{{route('pins_store_path')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="type">Type</label>
                                            <input type="text" class="form-control" name="type" id="type" placeholder="Type" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="taille">Taille</label>
                                            <select class="form-control" name="taille" id="taille" required>
                                                <option value="12 m&sup2">12 m&sup2</option>
                                                <option value="18 m&sup2">18 m&sup2</option>
                                                <option value="21 m&sup2">21 m&sup2</option>
                                                <option value="60 m&sup2">60 m&sup2</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="annonceur">Etat</label>
                                            <input type="text" class="form-control" name="etat" id="etat" placeholder="Etat" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="annonceur">Annonceur</label>
                                            <select name="annonceur" required id="annonceur" class="form-control">
                                                @if(count($annonceurs) > 0)
                                                    @foreach($annonceurs as $annonce)
                                                        <option value="{{$annonce->id}}">{{$annonce->libelle}}</option>
                                                    @endforeach
                                                @else
                                                    <option value="0">Inconnu</option>
                                                @endif
                                            </select>
                                            --}}{{--<input type="text" class="form-control" name="annonceur" id="annonceur" placeholder="Annonceur" >--}}{{--
                                        </div>
                                        <div class="form-group">
                                            <label for="activ">Secteur d'activité</label>
                                            <input type="text" class="form-control" name="activ" id="activ" placeholder="Secteur d'activité" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="marque">Marque</label>
                                            <input type="text" class="form-control" name="marque" id="marque" placeholder="Marque" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="agence">Agence Pub</label>
                                            <input type="text" class="form-control" name="agence" id="agence" placeholder="Agence Publicitaire" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="regie">Régie Pub</label>
                                            <input type="text" class="form-control" name="regie" id="regie" placeholder="Régie Publicitaire" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="note">Note</label>
                                            <div class="br-wrapper br-theme-fontawesome-stars">
                                                <select id="example-fontawesome" name="note" autocomplete="off" style="display: none;">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div id="mappins" style="height:200px;"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-9"></div>
                                                <div class="col-md-3">
                                                    <button type="button" id="shows" onclick="showLatLon()" class="btn btn-success btn-xs"><i class="fa fa-pencil" style="margin: 0px"></i></button>
                                                    <button type="button" id="mask" onclick="maskLatLon()" style="display: none" class="btn btn-light btn-xs"><i class="fa fa-eye-slash" style="margin: 0px"></i></button>
                                                    <span style="margin-bottom: 3px"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="lat">Latitude</label>
                                                    <input type="text" class="form-control" name="lat" id="lat" required readonly>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="lng">Longitude</label>
                                                    <input type="text" class="form-control" name="lng" id="lng" required readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="file">Photo du panneau </label>
                                            <input type="file" id="file" name="file" class="dropify" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-map-marker"></i> Ajouté</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>--}}

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Ref.</th>
                                    <th>Etat</th>
                                    <th>Update</th>
                                    <th>Type</th>
                                    <th>Taille</th>
                                    <th>Etat</th>
                                    <th>Annonceur</th>
                                    <th>Secteur activité</th>
                                    <th>Marque</th>
                                    <th>Agence</th>
                                    <th>Régie</th>
                                    <th>Pigiste</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @if(!empty($pins))
                                    <tbody>
                                        @foreach($pins as  $k =>$pin)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>
                                                @if($pin->actif==0)
                                                    <label class="badge badge-warning">Attente de validation</label>
                                                @elseif($pin->actif==1)
                                                    <label class="badge badge-success">valider</label>
                                                @else
                                                    <label class="badge badge-danger">Rejete</label>
                                                @endif
                                            </td>
                                            <td>{{$pin->updated_at->diffForHumans()}}</td>
                                            <td>{{$pin->type}}</td>
                                            <td>{!!$pin->taillePins->taille !!}</td>
                                            <td>{{$pin->etat}}</td>
                                            <td>{{$pin->annonceurPins->libelle}}</td>
                                            <td>{{$pin->sectActiv}}</td>
                                            <td>{{$pin->marque}}</td>
                                            <td>{{$pin->agence}}</td>
                                            <td>{{$pin->regie}}</td>
                                            <td>{{$pin->userPins->name}} {{$pin->userPins->prenoms}}</td>
                                            <td>
                                                <a href="{{route('profil_path',$pin->slug)}}" class="btn btn-outline-primary"><i class="mdi mdi-eye text-primary"></i> </a>
                                                <a href="{{route('pins_edit_path',$pin->slug)}}" class="btn btn-outline-success"><i class="fa fa-edit text-info"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/alerts.js')}} "></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //initialize();
            document.body.className='sidebar-icon-only';
        });
    </script>
@endsection
