@extends('layouts.app')

@section('title')
    Home
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/sweetalert/sweetalert.css')}}">
    <link rel="stylesheet" href="{{ asset('css/mystyle.css')}}">
@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Bienvenu</h4>
                    <div class="d-flex align-items-center">
                        <div class="wrapper mr-4 d-none d-sm-block">
                            <p class="mb-0">{{date('D')}} {{date('d')}}
                                <b class="mb-0">{{date('M')}} {{date('Y')}}</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group d-flex">

                                        {{--RECHERCHE MINITIEUX--}}
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1" id="dropform" style="width:1050px;padding: 20px">
                                                <form id="formMiniSearch" name="formMiniSearch" method="post" autocomplete="off">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="campagne">Campagne</label><br>
                                                                <input id="campagne" class="form-control" name="campagne">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="agence">Agence Pub</label>
                                                                <input type="text" class="form-control" name="agence" id="agence">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="regie">Régie Pub</label>
                                                                <input type="text" class="form-control" name="regie" id="regie">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="annonceur">Etat</label>
                                                                <select class="form-control" id="etat" name="etat">
                                                                    <option value="PROPRE">PROPRE</option>
                                                                    <option value="DEGRADE">DEGRADE</option>
                                                                    <option value="ENCOMBRE">ENCOMBRE</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="activ">Secteur d'activité</label>
                                                                <input type="text" class="form-control" name="activ" id="activ">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="marque">Marque</label>
                                                                <input type="text" class="form-control" name="marque" id="marque">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="type">Eclairage</label>
                                                                <select class="form-control" id="eclaire" name="eclaire">
                                                                    <option value="non">Non</option>
                                                                    <option value="oui">Oui</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="annonceur">Annonceur</label>
                                                                <select class="form-control" id="annonceur" name="annonceur">
                                                                    <option>Annonceur</option>
                                                                    @if(count($annonceurs) > 0)
                                                                        @foreach($annonceurs as $annonce)
                                                                            <option value="{{$annonce->libelle}}">{{$annonce->libelle}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="taille">Taille</label>
                                                                <select class="form-control" id="taille" name="taille">
                                                                    <option>Taille</option>
                                                                    @if(count($tailles) > 0)
                                                                        @foreach($tailles as $taille)
                                                                            <option value="{{$taille->taille}}">{!! $taille->taille !!}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="type">Type</label>
                                                                <select id="type" name="type" class="form-control">
                                                                    <option value="panneaux standard">Panneaux standard</option>
                                                                    <option value="panneaux numeriques">Panneaux numériques</option>
                                                                    <option value="panneaux double">Panneaux double</option>
                                                                    <option value="triples panneaux">Triples panneaux</option>
                                                                    <option value="grands panneaux">Grands panneaux</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{ csrf_field()}}
                                                    <button type="button" style="float: right;margin-top: 15px;" class="btn btn-success" onclick="miniSearch()" value="Rechercher" ><span class="fa fa-search" aria-hidden="true"></span> Rechercher</button>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- END RECHERCHE MINITIEUX--}}

                                        <input id="pac-input" type="text" class="form-control" placeholder="Rechercher">
                                        <a style="color: #FFF" id="searchForm" onclick="searchForm()" class="btn btn-success ml-3"><i class="mdi mdi-magnify"></i> Rechercher</a>
                                        <a style="color: #FFF" id="mjabtn" onclick="searchMJA()" class="btn btn-primary ml-3"><i class="mdi mdi-magnify"></i> MAJ</a>
                                        <div style="margin-left: 10px;display:none;height: auto;" id="loader" class="bar-loader myload"><span></span><span></span><span></span><span></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="map-container">
                        <div id="map" class="google-map"></div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="urlwebsite" id="urlwebsite" value="{{url('piges/marqueurs/')}}">
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')

    </div>
    <!-- main-panel ends -->
@endsection



@section('footer_scripts')

    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js')}}"></script>
    <!-- End custom js for this page-->

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo0BJVrs4uoShGageKqEVvfSE0a2Kwqo8"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/js-marker-clusterer/1.0.0/markerclusterer_compiled.js"></script>--}}
    <script type="text/javascript" src="{{asset('js/myscript.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            document.body.className='sidebar-icon-only';
            if (typeof google === 'object' && typeof google.maps === 'object') {
                console.log('bien charge');
            }else{
                swal("Désolé ", "La periode d'attente est dépassé. Vérifié votre connexion internet", "error")
                return false;
                console.log('pas bien charge');
            }
        });

        $("#dropdownMenuIconButton1").click(function(){
            $('#dropform').slideToggle("slow");
        });

        function searchForm() {
            var searchval=$("#pac-input").val();
            if (searchval == "") {
                swal("Désolé ", "Veuillez renseigner cet champs !", "error")
                return false;
            }
            $('#searchForm').hide();
            $('#mjabtn').hide();
            //$('#loader').show();

            $.ajax
            ({
                type: "get",
                url:'{{ route("searchPinad") }}',
                timeout: 10000,
                data: "valueSearch="+searchval,
                beforeSend: function() {
                    $('#loader').show();
                },
                success: function(response) {
                    var rep = JSON.parse(response);

                    var data = rep.data;
                    var nbdata = rep.data.length;

                    if(nbdata>0){
                        //console.log(data);
                        $("#map").empty();
                        mycreatemap();

                        data.forEach(function(value){
                            var lat = value.lat;
                            var lon = value.lng;
                            var title = value.libelle;
                            var urltrue= $("#urlwebsite").val();
                            var icon = urltrue+'/'+value.marqueur;

                            var LatLng = new google.maps.LatLng(lat,lon);
                            var date = new Date(value.updated_at),
                                yr      = date.getFullYear(),
                                month   = date.getMonth(),
                                day     = date.getDate(),
                                newDate = day+'-'+month+'-'+yr;

                            var contentString = '<div style="width: 410px;">' +
                                '<div class="">' +
                                '<div class="d-flex flex-row">' +
                                '<img src="piges/panneau/'+value.panneau+'" class="img-lgM rounded" alt="image panneau">' +
                                '<div class="ml-3">' +
                                '<p class="mt-2 text-success font-weight-bold">' + value.libelle + '</p>' +
                                '<div class="cardMe-text">Marque : <b>' + value.marque + '</b> <br> Régie Pub : <b>' + value.regie + '</b> <br> Taille : <b>' + value.taille + '</b></div>' +
                                '<div class="cardMe-footer"><small>Mise à jour le '+newDate+'</small><br><label class="badge badge-success" style="margin-top: 10px"><a href="' + value.profile_url +'" style="text-decoration: none;color: #FFF;"><i class="fa fa-eye"></i> Voir plus</a></label></div>' +
                                '</div></div></div></div>';
                            //var contentString = '<a class="special" href="#"><span><b>' + item.bio + '</b></span></a>';
                            allMarkers.push(createMarker(LatLng,icon,title,contentString));
                        });

                    }else{
                        //alert('une erreur est survenu veuiller relancer la recherche');
                        allClients();
                        swal("Désolé ", "Veuiller rechercher par une marque ou régie ", "error");
                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                    }
                    $('#searchForm').show();
                    $('#mjabtn').show();
                    $('#loader').hide();
                }
            });

        }

        function searchMJA() {

            $('#searchForm').hide();
            $('#mjabtn').hide();
            $('#loader').show();

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success,fail);
            }else{
                swal("Désolé ", "Le navigateur ne supporte pas la géolocalisation", "error");
                $('#searchForm').show();
                $('#mjabtn').show();
                $('#loader').hide();
                return false;
            }

            function success(position){
                console.log(position);
                lat = position.coords.latitude;
                lon = position.coords.longitude;

                $.ajax
                ({
                    type: "get",
                    url:'{{ route("searchPinmja") }}',
                    timeout: 10000,
                    data: "lat="+lat+'&lon='+lon,
                    beforeSend: function() {
                        $('#mjabtn').hide();
                    },
                    success: function(response) {
                        var rep = JSON.parse(response);
                        console.log(rep);
                        var data = rep.data;
                        var nbdata = rep.data.length;
                        //console.log(data);
                        //alert("stop");
                        if(nbdata>0){
                            //console.log(data);
                            $("#map").empty();

                            //CREATION DE MA MAP
                            map = new google.maps.Map(document.getElementById('map'), {
                                center: new google.maps.LatLng(lat,lon),
                                scrollwheel:true,
                                zoom: 20,
                            });

                            var LatLng = new google.maps.LatLng(lat,lon);

                            var marker = new google.maps.Marker({
                                position: LatLng,
                                map: map,
                                animation: google.maps.Animation.DROP,
                                title: 'je suis ici'
                            });


                            data.forEach(function(value){
                                var lat = value.lat;
                                var lon = value.lng;
                                var title = value.libelle;
                                var urltrue= $("#urlwebsite").val();
                                var icon = urltrue+'/'+value.marqueur;

                                var LatLng = new google.maps.LatLng(lat,lon);
                                var date = new Date(value.updated_at),
                                    yr      = date.getFullYear(),
                                    month   = date.getMonth(),
                                    day     = date.getDate(),
                                    newDate = day+'-'+month+'-'+yr;

                                var contentString = '<div style="width: 410px;">' +
                                    '<div class="">' +
                                    '<div class="d-flex flex-row">' +
                                    '<img src="piges/panneau/'+value.panneau+'" class="img-lgM rounded" alt="image panneau">' +
                                    '<div class="ml-3">' +
                                    '<p class="mt-2 text-success font-weight-bold">' + value.libelle + '</p>' +
                                    '<div class="cardMe-text">Marque : <b>' + value.marque + '</b> <br> Régie Pub : <b>' + value.regie + '</b> <br> Taille : <b>' + value.taille + '</b></div>' +
                                    '<div class="cardMe-footer"><small>Mise à jour le '+newDate+'</small><br>' +
                                    '<label class="badge badge-success" style="margin-top: 10px"><a href="' + value.profile_url +'" style="text-decoration: none;color: #FFF;"><i class="fa fa-eye"></i> Voir plus</a></label>' +
                                    '<label class="badge badge-danger" style="margin-top: 10px;margin-left: 10px"><a href="' + value.edit_url +'" style="text-decoration: none;color: #FFF;"><i class="fa fa-edit"></i> Modifier</a></label>' +
                                    '</div>' +
                                    '</div></div></div></div>';
                                //var contentString = '<a class="special" href="#"><span><b>' + item.bio + '</b></span></a>';
                                allMarkers.push(createMarker(LatLng,icon,title,contentString));
                            });

                        }else{
                            //alert('une erreur est survenu veuiller relancer la recherche');
                            //allClients();
                            swal("Désolé ", "Aucun panneau n'a été trouver ", "error");
                            $('#searchForm').show();
                            $('#mjabtn').show();
                            $('#loader').hide();
                        }

                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                    }
                });

            }

            function fail(error){
                console.log(error);
                switch(error.code) {
                    case error.PERMISSION_DENIED:
                        swal("Désolé ", "Vous avez annuler la géolocalisation de cet document", "error")
                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                        break;
                    case error.POSITION_UNAVAILABLE:
                        swal("Désactiver ", "Information de géolocalisation indéfini. Vérifié votre connexion internet", "error")
                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                        break;
                    case error.TIMEOUT:
                        swal("Temps ", "La periode d'attente est dépassé. Vérifié votre connexion internet", "error")
                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                        break;
                    case error.UNKNOWN_ERROR:
                        swal("Erreur ", "Erreur inconnue. Vérifié votre connexion internet", "error")
                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                        break;
                }
            }

            //console.log(searchval);
        }

        function miniSearch() {

            $(".dropdown-menu").hide();
            $('#searchForm').hide();
            $('#mjabtn').hide();

            $.ajax
            ({
                type: "post",
                url: "{{route('searchmini')}}",
                timeout: 60000,
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                data: new FormData($("#formMiniSearch")[0]),

                beforeSend: function() {
                    $('#loader').show();
                },

                success: function (response) {
                    var rep = JSON.parse(response);
                    var data = rep.data;
                    var nbdata = rep.data.length;

                    if(nbdata>0){
                        console.log(data);
                        $("#map").empty();
                        mycreatemap();

                        data.forEach(function(value){
                            var lat = value.lat;
                            var lon = value.lng;
                            var title = value.libelle;
                            var urltrue= $("#urlwebsite").val();
                            var icon = urltrue+'/'+value.marqueur;

                            var LatLng = new google.maps.LatLng(lat,lon);
                            var date = new Date(value.updated_at),
                                yr      = date.getFullYear(),
                                month   = date.getMonth(),
                                day     = date.getDate(),
                                newDate = day+'-'+month+'-'+yr;

                            var contentString = '<div style="width: 410px;">' +
                                '<div class="">' +
                                '<div class="d-flex flex-row">' +
                                '<img src="piges/panneau/'+value.panneau+'" class="img-lgM rounded" alt="image panneau">' +
                                '<div class="ml-3">' +
                                '<p class="mt-2 text-success font-weight-bold">' + value.libelle + '</p>' +
                                '<div class="cardMe-text">Marque : <b>' + value.marque + '</b> <br> Régie Pub : <b>' + value.regie + '</b> <br> Taille : <b>' + value.taille + '</b></div>' +
                                '<div class="cardMe-footer"><small>Mise à jour le '+newDate+'</small><br><label class="badge badge-success" style="margin-top: 10px"><a href="' + value.profile_url +'" style="text-decoration: none;color: #FFF;"><i class="fa fa-eye"></i> Voir plus</a></label></div>' +
                                '</div></div></div></div>';
                            //var contentString = '<a class="special" href="#"><span><b>' + item.bio + '</b></span></a>';
                            allMarkers.push(createMarker(LatLng,icon,title,contentString));
                        });

                    }else{
                        //alert('une erreur est survenu veuiller relancer la recherche');
                        allClients();
                        swal("Désolé ", "Veuiller rechercher par une marque ou régie ", "error");
                        $('#searchForm').show();
                        $('#mjabtn').show();
                        $('#loader').hide();
                    }
                    $('#searchForm').show();
                    $('#mjabtn').show();
                    $('#loader').hide();

                },

            });
        }

    </script>
@endsection
