<!DOCTYPE html>
<html lang="fr">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Créer mon compte | Projet XP</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/iconfonts/simple-line-icon/css/simple-line-icons.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/iconfonts/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.addons.css')}}">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{ asset('css/style.css')}}">
        <!-- endinject -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png')}}" />
    </head>

    <body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper auth p-0 theme-two">
                <div class="row d-flex align-items-stretch">
                    <div class="col-md-4 banner-section d-none d-md-flex align-items-stretch justify-content-center">
                        <div class="slide-content bg-2">
                        </div>
                    </div>
                    <div class="col-12 col-md-8 h-100 bg-white">
                        <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">
                            <div class="nav-get-started">
                                <p>Vous avez déjà un compte?</p>
                                <a class="btn get-started-btn" href="{{route('login')}}">SE CONNECTER</a>
                            </div>
                            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <h3 class="mr-auto">Créer mon compte</h3>
                                <p class="mb-5 mr-auto">Entrez vos coordonnées ci-dessous.</p>
                                @include('includes/successOrerror')
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-account-outline"></i></span>
                                        </div>
                                        <input name="nom" id="nom" type="text" class="form-control" placeholder="Nom" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-account-outline"></i></span>
                                        </div>
                                        <input name="email" id="email" type="email" class="form-control" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-lock-outline"></i></span>
                                        </div>
                                        <input name="pass" id="pass" type="password" class="form-control" placeholder="Mot de passe" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-lock-outline"></i></span>
                                        </div>
                                        <input name="passconfirme" id="passconfirme" type="password" class="form-control" placeholder="Confirmer le mot de passe" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary submit-btn">CREER MON COMPTE</button>
                                </div>
                                <div class="wrapper mt-5 text-gray">
                                    <p class="footer-text">Copyright © 2018 <a href="#" target="_blank">Project XP</a>. All rights reserved.</p>
                                    <ul class="auth-footer text-gray">
                                        <li><a href="#">Terms & Conditions</a></li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{ asset('vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js')}}"></script>
    <script src="{{ asset('js/hoverable-collapse.js')}}"></script>
    <script src="{{ asset('js/misc.js')}}"></script>
    <script src="{{ asset('js/settings.js')}}"></script>
    <script src="{{ asset('js/todolist.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <!-- End custom js for this page-->
    </body>

    </html>