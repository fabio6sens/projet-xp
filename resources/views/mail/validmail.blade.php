<html>
<head>
    <style>
        @media  only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #ffffff; color: #000; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">

<table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class="bgBody">
    <tbody><tr>
        <td>
            <table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
                <tbody><tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                            <tbody><tr>
                                <td class="movableContentContainer bgItem">

                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tbody><tr height="40">
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" valign="top">&nbsp;</td>
                                                <td width="200" valign="top" align="center">
                                                    <div class="contentEditableContainer contentImageEditable">
                                                        <div class="contentEditable" align="center">
                                                            <img src="{{ URL::asset('images/logoold.png')}}" height="155" alt="Logo" data-default="placeholder">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="200" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr height="25">
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                            </tbody></table>
                                    </div>

                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="font-size: 20px">
                                            <tbody>
                                            <tr>
                                                <td width="10">&nbsp;</td>
                                                <td width="400" align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align="left">
                                                            <br>
                                                            <p><strong>Bonjour {{$nameUser}}, </strong>
                                                                <br>
                                                                <br>
                                                                <p style="line-height: 1.5">S'il vous plaît cliquer sur le bouton « confirmer mon compte » pour vérifier votre email</p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                            </tr>
                                            </tbody></table>
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tbody><tr>
                                                <td width="200">&nbsp;</td>
                                                <td width="200" align="center" style="padding-top:25px;">
                                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="200" height="50">
                                                        <tbody><tr>
                                                            <td bgcolor="#3097D1" align="center" style="border-radius:4px;" width="200" height="50">
                                                                <div class="contentEditableContainer contentTextEditable">
                                                                    <div class="contentEditable" align="center">
                                                                        <a target="_blank" href="{{ URL::to('confirm/'.$iduser.'/'. urlencode($token)) }}" class="link2" style="color: #FFF;text-decoration: none;">CONFIRMER MON COMPTE</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                            </tbody></table>
                                    </div>

                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="font-size: 20px">
                                            <tbody>
                                            <tr>
                                                <td width="10">&nbsp;</td>
                                                <td width="400" align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align="left">
                                                            <br><br>
                                                            <p style="line-height: 1.5">
                                                                S'il vous plaît ignorer ce message si vous n'avez pas essayé de vous inscrire sur le site Gagner des appels d’offres ({{url('/')}}).
                                                                <!-- <hr style="margin-top: 30px"> -->
                                                            </p>
                                                        </div>
                                                        <div align="left" style="color: #74787E;font-size: 12px;"><p>
                                                                Si vous rencontrez des difficultés pour cliquer sur le bouton "Confirmer mon compte", copiez et collez l'URL ci-dessous dans votre navigateur {{ URL::to('confirm/'.$iduser.'/'. urlencode($token)) }}
                                                            </p>
                                                        </div>

                                                        <div align="left"><p>
                                                                Cordialement,<br>
                                                                Service Clientèle <br>
                                                                <a href="{{url('/')}}" target="_blank">Project XP</a>
                                                            </p>
                                                        </div>

                                                    </div>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                            </tr>
                                            </tbody></table>
                                    </div>


                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tbody><tr>
                                                <td width="100%" colspan="2" style="padding-top:65px;">
                                                    <hr style="height:1px;border:none;color:#333;background-color:#ddd;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="60%" height="70" valign="middle" style="padding-bottom:20px;">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align="left">
                                                            <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;">Envoyé par support@projetxp.com <br> PROJECT XP</span>
                                                            <br>
                                                            <span style="font-size:11px;color:#555;font-family:Helvetica, Arial, sans-serif;line-height:200%;">© 2018 project XP. All rights reserved.</span>
                                                            <br>
                                                            <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;"></span></div>
                                                    </div>
                                                </td>
                                                <td width="40%" height="70" align="right" valign="top" style="padding-bottom:20px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                        <tbody><tr>
                                                            <td width="57%"></td>
                                                            <td valign="top" width="34">
                                                                <div class="contentEditableContainer contentFacebookEditable" style="display:inline;">
                                                                    <div class="contentEditable">
                                                                        <a href="#" target="_blank"><img src="{{ URL::asset('images/facebook.png')}}" data-default="placeholder" data-max-width="30" data-customicon="true" width="30" height="30" alt="facebook" style="margin-right:40x;"></a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td valign="top" width="34">
                                                                <div class="contentEditableContainer contentTwitterEditable" style="display:inline;">
                                                                    <div class="contentEditable">
                                                                        <a href="#" target="_blank"><img src="{{ URL::asset('images/twitter.png')}}" data-default="placeholder" data-max-width="30" data-customicon="true" width="30" height="30" alt="twitter" style="margin-right:40x;"></a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            </tbody></table>




                    </td></tr></tbody></table>

        </td>
    </tr>
    </tbody></table>



</body></html>
