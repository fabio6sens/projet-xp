@extends('layouts.app')

@section('title')
    Taille des panneaux
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Gestion des tailles panneaux</h4>
                    <div class="d-flex align-items-center">
                        <div class="wrapper">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal-4"><i class="fa fa-signal"></i> Ajouter une taille</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4"><i class="fa fa-drivers-license-o"></i> Nouveau pins</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="forms-sample" action="{{route('size_store_path')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="type">Taille</label>
                                            <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="file">Marqueur </label>
                                            <input type="file" id="file" name="file" class="dropify" required/>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-drivers-license-o"></i> Ajouté</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{--#################################### Version Update Modal #############################################--}}
            <div class="modal fade" id="versionUpdateModal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-sd">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-4"><i class="fa fa-drivers-license-o"></i> Mise à jour</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="formsUpdate" action="{{route('size_update_path')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="type">Taille</label>
                                            <input type="text" class="form-control" name="libelle" id="libelle" placeholder="Nom" required>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6" id="markerImg">

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="file">Nouveau marqueur</label>
                                                    <input type="file" id="file" name="file" class="dropify"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="slug" id="slug">
                                <button type="submit" id="updateBtn" class="btn btn-success" style="display: none"><i class="fa fa-drivers-license-o"></i> Modifié</button>
                                <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{--#################################### Version Update Modal #############################################--}}
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Ref.No.#</th>
                                    <th>Marqueur</th>
                                    <th>Taille</th>
                                    <th>Date d'ajout</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @if(!empty($tailles))
                                    <tbody>
                                        @foreach($tailles as  $k =>$taille)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td class="py-1">
                                                <img src="{{ $taille->marqueur  ? url('piges/marqueurs/'.$taille->marqueur.'') : '' }}" alt="marqueur">
                                            </td>
                                            <td>{!! $taille->taille  !!}</td>

                                            <td>{{$taille->created_at->format('d-m-Y')}}</td>
                                            <td>
                                                <a href="#" class="btn btn-outline-success" aria-expanded="false" title="Modifier" id="modifier" data-toggle="modal" onclick="displayUpdate('{{$taille->id}}')" data-target="#versionUpdateModal"><i class="fa fa-edit text-info"></i> Edité</a>
                                                {{--<a href="" class="btn btn-outline-success"><i class="fa fa-edit text-info"></i> Edité</a>--}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/x-editable.js')}}"></script>
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/jquery-file-upload.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/alerts.js')}} "></script>

    <script type="text/javascript">
        function displayUpdate(id) {
            $("#formsUpdate").empty();
            $("#markerImg").empty();
            $("#slug").val(id);
            $.ajax
            ({
                type: "get",
                url: "{{ route('size_info_path') }}",
                timeout: 10000,
                data: 'id=' + id ,
                success: function (response) {
                    $("#updateBtn").show();
                    var rep = JSON.parse(response);
                    console.log(rep);
                    $("#libelle").val(rep.data[0].libelle);
                    var img='<img src="piges/marqueurs/'+rep.data[0].marqueur+'" alt="old marker">';
                    $("#markerImg").append(img);
                }
            });

        }
    </script>

@endsection
