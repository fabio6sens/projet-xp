@extends('layouts.app')

@section('title')
    Confirguration
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="row mb-4">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h4 class="page-title">Gérer ma confirguration</h4>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @if($annonceurs->count()>0)
                            @foreach($annonceurs as$annonceur)
                                <div class="col-md-4">
                                    <div id="dragula-event-left" class="py-2">
                                        <div class="card rounded border mb-2">
                                            <div class="card-body p-3">
                                                <div class="media">
                                                    <i class="mdi mdi-check icon-sm text-primary align-self-center mr-3"></i>
                                                    <div class="media-body row">
                                                        <div class="col-md-6">
                                                            <h4 class="mb-1">{{$annonceur->libelle}}</h4>
                                                            {{--<p class="mb-0 text-muted">
                                                                Build wireframe for the new app
                                                            </p>--}}
                                                        </div>
                                                        <div class="col-md-4">

                                                            @foreach($annonceur->userAnnonceur()->pluck('user_id') as $user)
                                                                @if($user == Auth::user()->id)
                                                                    <a href="#" class="btn btn-success btn-fw"><i class="fa fa-bell-o"></i>Abonné</a>
                                                                @else
                                                                    <a href="{{route('add_path',[$annonceur->id,$annonceur->libelle])}}" class="btn btn-secondary btn-fw">
                                                                        <i class="fa fa-bell-o"></i>Suivre
                                                                    </a>
                                                                @endif
                                                            @endforeach


                                                            {{--<button type="button" class="btn btn-secondary btn-fw"><i class="fa fa-bell-o"></i>Suivre</button>
                                                            <button type="button" class="btn btn-success btn-fw"><i class="fa fa-bell-o"></i>Abonné</button>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <nav>
                    @if(!empty($annonceurs))
                        {{--Ma pagination--}}
                        {{$annonceurs->links()}}
                    @endif
                </nav>

            </div>

        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/dragula.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>

@endsection
