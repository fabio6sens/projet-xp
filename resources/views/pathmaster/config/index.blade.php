@extends('pathmaster.layouts.app')

@section('title')
    Confirguration
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="container">
                @include('includes.successOrerror')
                <div class="row mb-4">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <h4 class="page-title">Gérer ma confirguration</h4>
                        <div class="d-flex align-items-center">
                            <div class="wrapper">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal-4"><i class="fa fa-bell-o"></i> Suivre un annonceur</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel-4"><i class="fa fa-bell-o"></i> Suivre un annonceur</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form class="forms-sample" action="{{route('add_path')}}" method="POST" autocomplete="off">
                                {{csrf_field()}}
                                @if(count($annonceursNotUser)>0)
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Selectionnez les annonceus</label>
                                                    <select class="js-example-basic-multiple w-100" name="ann[]" multiple="multiple" required>
                                                        @foreach($annonceursNotUser as $annonceur)
                                                            <option value="{{$annonceur->id}}">{{$annonceur->libelle}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-bell-o"></i> Suivre</button>
                                        <button type="reset" class="btn btn-light" data-dismiss="modal">Ferme</button>
                                    </div>
                                @else
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p>Oups! Vous suivez tous les annonceurs</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            @if($annonceurs->count()>0)
                                @foreach($annonceurs as$annonceur)
                                    <div class="col-md-4">
                                        <div id="dragula-event-left" class="py-2">
                                            <div class="card rounded border mb-2">
                                                <div class="card-body p-3">
                                                    <div class="media">
                                                        <i class="mdi mdi-check icon-sm text-primary align-self-center mr-3"></i>
                                                        <div class="media-body row">
                                                            <div class="col-md-6">
                                                                <h4 class="mb-1">{{$annonceur->libelle}}</h4>
                                                                {{--<p class="mb-0 text-muted">
                                                                    Build wireframe for the new app
                                                                </p>--}}
                                                            </div>
                                                            <div class="col-md-4">

                                                                {{--<a onmouseenter="hidebtn(this)" onmouseleave="normalImg(this)" id="btn-abon" href="#" class="btn btn-success btn-fw"><i class="fa fa-bell-o"></i>Abonné</a>--}}
                                                                <a id="btn-del" href="{{route('del_path',[$annonceur->id,$annonceur->libelle])}}" class="btn btn-danger btn-fw"><i class="fa fa-bell-o"></i>Se désabonner</a>
                                                                {{--<a href="{{route('add_path',[$annonceur->id,$annonceur->libelle])}}" class="btn btn-secondary btn-fw">
                                                                    <i class="fa fa-bell-o"></i>Suivre
                                                                </a>--}}

                                                                {{--<button type="button" class="btn btn-secondary btn-fw"><i class="fa fa-bell-o"></i>Suivre</button>
                                                                <button type="button" class="btn btn-success btn-fw"><i class="fa fa-bell-o"></i>Abonné</button>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="container text-center">
                                    <h4 class="mb-3 mt-5">Oups!!</h4>
                                    <p class="w-75 mx-auto mb-5">Vous etes abonnés à aucun annonceur</p>
                                </div>
                            @endif
                        </div>
                    </div>

                    <nav>
                        @if(!empty($annonceurs))
                            {{--Ma pagination--}}
                            {{$annonceurs->links()}}
                        @endif
                    </nav>

                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        @include('includes.footer')
    </div>

@endsection

@section('footer_scripts')
    <script src="{{ asset('js/data-table.js')}} "></script>
    <script src="{{ asset('js/formpickers.js')}} "></script>
    <script src="{{ asset('js/form-addons.js')}}"></script>
    <script src="{{ asset('js/dragula.js')}}"></script>
    <script src="{{ asset('js/form-repeater.js')}} "></script>
    <script src="{{ asset('js/select2.js')}} "></script>

    <script type="text/javascript">

        $(document).ready(function () {
            /*$(document).on('mouseenter', '#btn-abon', function () {
                //console.log('on');
                //$(this).find("#btn-abon").show();
                //$(this).find("#btn-del").hide();
                $("#btn-abon").show();
                $("#btn-del").hide();
            }).on('mouseleave', '#btn-del', function () {
                //console.log('off');
                $("#btn-del").hide();
                $("#btn-abon").show();
            });*/
        });
    </script>

@endsection
