@extends('pathmaster.layouts.app')

@section('title')
    Profil
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="container">
                @include('includes/successOrerror')
                <div class="row profile-page">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="profile-header text-white d-none d-md-block">
                                    <div class="d-flex justify-content-around">
                                        <div class="profile-info d-flex justify-content-center align-items-md-center">
                                            <img class="rounded-circle img-lg" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}" alt="image de profil">
                                            <div class="wrapper pl-4">
                                                <p class="profile-user-name">{{Auth::user()->name}} {{Auth::user()->prenoms}}</p>
                                                <div class="wrapper d-flex align-items-center">
                                                    {{--<p class="profile-user-designation">User Experience Specialist</p>--}}
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="details">
                                            <div class="detail-col ">
                                                <p>Projects</p>
                                                <p>130</p>
                                            </div>
                                            <div class="detail-col ">
                                                <p>Projects</p>
                                                <p>130</p>
                                            </div>
                                        </div>--}}
                                    </div>
                                </div>
                                <div class="profile-body pt-0 pt-sm-4">
                                    {{--<ul class="nav tab-switch " role="tablist">
                                        <li class="nav-item ">
                                            <a class="nav-link active " id="user-profile-info-tab " data-toggle="pill " href="#user-profile-info" role="tab " aria-controls="user-profile-info " aria-selected="true ">Profile</a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link " id="user-profile-activity-tab " data-toggle="pill " href="#user-profile-activity" role="tab " aria-controls="user-profile-activity " aria-selected="false ">Activity</a>
                                        </li>
                                    </ul>--}}
                                    <ul class="nav tab-switch" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#whoweare" role="tab" aria-controls="whoweare" aria-selected="true">Profil</a>
                                        </li>
                                        {{--<li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ourgoal" role="tab" aria-controls="ourgoal" aria-selected="false">Modifier mon profil</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">Modifier mon mot de passe</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar" aria-selected="false">Modifier mon avatar</a>
                                        </li>--}}
                                    </ul>

                                    <div class="tab-content tab-content-basic">
                                        <div class="tab-pane fade show active" id="whoweare" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="table-responsive ">
                                                <table class="table table-borderless w-100 mt-4 ">
                                                    <tr>
                                                        <td>
                                                            <strong>Entreprise :</strong> {{Auth::user()->name}}</td>
                                                        <td>
                                                            <strong>Email :</strong> {{Auth::user()->email}}</td>
                                                            {{--<strong>Prénoms :</strong> {!! Auth::user()->prenoms ? Auth::user()->prenoms : '<span class="card-description">Non specifier</span>' !!}</td>--}}
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Adresse :</strong> {!! Auth::user()->address ? Auth::user()->address : '<span class="card-description">Non specifier</span>' !!}</td>
                                                        <td>
                                                            <strong>Contact :</strong> {{Auth::user()->contact}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Abonnement :</strong> Votre abonnement expire le <span class="text-danger">31/12/2018</span></td>
                                                        <td>
                                                            </td>
                                                    </tr>
                                                    {{--<tr>
                                                        <td>
                                                            <strong>Sexe :</strong> {{Auth::user()->sexe}}</td>
                                                        <td>
                                                            <strong>Contact :</strong> {{Auth::user()->contact}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Bio :</strong>
                                                            <textarea class="form-control" name="" id="" rows="5">{{Auth::user()->bio}}</textarea>
                                                        </td>
                                                    </tr>--}}
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ourgoal" role="tabpanel" aria-labelledby="profile-tab">
                                            <div style="padding: 2%">
                                                <form class="form-sample" method="post" action="{{route('updateclt')}}">
                                                    {{csrf_field()}}
                                                    <p class="card-description">
                                                        Information Personnel
                                                    </p>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Nom</label>
                                                                <div for="nom" class="col-sm-9">
                                                                    <input type="text" value="{{Auth::user()->name}}" id="nom" name="nom" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="phone" class="col-sm-3 col-form-label">Contact</label>
                                                                <div class="col-sm-9">
                                                                    <input value="{{Auth::user()->contact}}" id="phone" name="phone" class="form-control" placeholder="Contact">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--<div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="prenoms" class="col-sm-3 col-form-label">Prénoms</label>
                                                                <div class="col-sm-9">
                                                                    <input value="{{Auth::user()->prenoms}}" id="prenoms" name="prenoms" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>--}}
                                                    </div>
                                                    {{--<div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="sexe" class="col-sm-3 col-form-label">Sexe</label>
                                                                <div class="col-sm-9">
                                                                    <select class="form-control" name="sexe" id="sexe" required>
                                                                        <option {{Auth::user()->sexe=='Homme' ? 'selected' : ''}} value="Homme">Homme</option>
                                                                        <option {{Auth::user()->sexe=='Femme' ? 'selected' : ''}} value="Femme">Femme</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>--}}
                                                    <br>
                                                    <p class="card-description">
                                                        Information Géographique
                                                    </p>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="address" class="col-sm-3 col-form-label">Addresse</label>
                                                                <div class="col-sm-9">
                                                                    <input value="{{Auth::user()->address}}" id="address" name="address" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--<div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="bio" class="col-sm-3 col-form-label">Description</label>
                                                                <div class="col-sm-9">
                                                                    <textarea class="form-control" name="bio" id="bio" rows="5">{{Auth::user()->bio}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>--}}
                                                    </div>

                                                    <hr>
                                                    <button type="submit" class="btn btn-success mr-2"><i class="fa fa-save"></i> Enregistrer</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="contact-tab">
                                            <div style="padding: 2%">
                                                <form class="forms-sample" method="post" action="{{route('updateuserpassclt')}}">
                                                    {{csrf_field()}}
                                                    <div class="form-group">
                                                        <label for="motpass">Mot de passe courant</label>
                                                        <input type="password" class="form-control" id="motpass" name="motpass" placeholder="Mot de passe courant">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="newpass">Nouveau mot de passe</label>
                                                        <input type="password" class="form-control" id="newpass" name="newpass" placeholder="Nouveau mot de passe">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="confirmpass">Confirmer le mot de passe</label>
                                                        <input type="password" class="form-control" id="confirmpass" name="confirmpass" placeholder="Confirmer le mot de passe">
                                                    </div>
                                                    <button type="submit" class="btn btn-success mr-2"><i class="fa fa-save"></i> Enregistrer</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                                            <div style="padding: 2%">
                                                <form class="clearfix" action="{{route('updateuseravatarclt')}}" method="POST" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-4">
                                                                <div class="thumbnail">
                                                                    <img id="output" class="img-responsive" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}" alt="Image de profil" width="300" height="350" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 col-sm-1"></div>
                                                            <div class="col-md-8 col-sm-7">
                                                                <div class="sky-form nomargin">
                                                                    <label id="file">Selectionner une image</label>
                                                                    <input type="file" accept='image/*' id="file" name="fileUser" onchange="loadFile(event)" class="file-upload-default">
                                                                    <div class="input-group col-xs-12">
                                                                        <input type="text" class="form-control file-upload-info" disabled placeholder="Selectionner une image">
                                                                        <span class="input-group-append">
                                                                      <button class="file-upload-browse btn btn-info" type="button">Télécharger</button>
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="margiv-top10">
                                                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                                                        <button type="submit" name="imgProfil" class="btn btn-danger"><i class="fa fa-check"></i> Enregistrer </button>
                                                        <button type="reset" class="btn btn-default">Annuler </button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')

    </div>

@endsection



@section('footer_scripts')
    <script src="{{ asset('js/form-addons.js')}} "></script>
    <script src="{{ asset('js/file-upload.js')}} "></script>
    <script type="text/javascript">
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endsection
