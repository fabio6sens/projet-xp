@extends('pathmaster.layouts.app')

@section('title')
    Alerts
    @parent
@stop

@section('header_styles')

@endsection


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            @include('includes/successOrerror')
            <div class="container">
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <h4 class="card-title">Vos alertes</h4>
                                    <a href="{{route('readAllNotifs_path')}}" style="text-decoration: none">Tous marquer comme lu</a>
                                    {{--<button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuSizeButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dropdown
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton3">
                                        <h6 class="dropdown-header">Settings</h6>
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>--}}
                                </div>
                                <p class="card-description"></p>

                                @foreach($allnotif as $notif)
                                    <div id="notifs" class="list d-flex align-items-center border-bottom py-3 espaceme {{$notif->read_at == '0' ? 'pas_vue' : ''}}">
                                        <img class="img-sm rounded-circle" src="{{url('piges/panneau/'.$notif->panneau)}}" alt="Photo du panneau">
                                        <div class="wrapper w-100 ml-3">
                                            <p class="mb-0">
                                                <b>{{$notif->marque}} </b>posté par {{$notif->agence}}</p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex align-items-center">
                                                    <a id="showNotif" href="{{route('pin_profil',$notif->slug)}}" class="label label-primary" style="text-decoration: none">
                                                        <i class="fa fa-eye"></i> voir</a> {{--<input id="see" type="hidden" value="{{$notif->idnotif}}">--}}
                                                </div>
                                                <small class="text-muted ml-auto">{{date("d-m-Y H:i",strtotime($notif->dateM))}}</small>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        @include('includes.footer')

    </div>

@endsection



@section('footer_scripts')

@endsection
