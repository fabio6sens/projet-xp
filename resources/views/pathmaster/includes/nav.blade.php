<nav class="navbar horizontal-layout-2 col-lg-12 col-12 p-0 d-flex flex-row align-items-start">
    <div class="container">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a class="navbar-brand brand-logo" href="{{route('homeclt')}}">
                <img src="{{url('images/logo.png')}}" alt="logo" style="height: 60px;"/>
                {{--Projet XP--}}
            </a>
            <a class="navbar-brand brand-logo-mini" href="{{route('homeclt')}}">
                <img src="{{url('images/logo.png')}}" alt="logo" style="height: 60px;"/>
                {{--XP--}}
            </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center pr-0">
            {{--<ul class="navbar-nav header-links">
                <li class="nav-item">
                    <a href="#" class="nav-link">Schedule
                        <span class="badge badge-success ml-1">New</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a href="#" class="nav-link">
                        <i class="mdi mdi-elevation-rise"></i>Reports</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
                </li>
            </ul>--}}
            <ul class="navbar-nav ml-auto dropdown-menus">
                {{--<li class="nav-item dropdown">
                    <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-message-text-outline"></i>
                        <span class="count bg-warning">2</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                        <div class="dropdown-item py-3">
                            <p class="mb-0 font-weight-medium float-left">You have 7 unread mails
                            </p>
                            <span class="badge badge-inverse-info badge-pill float-right">View all</span>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                                <img src="https://www.placehold.it/36x36" alt="image" class="profile-pic">
                            </div>
                            <div class="preview-item-content flex-grow">
                                <h6 class="preview-subject ellipsis font-weight-normal text-dark mb-1">David Grey
                                    <span class="float-right font-weight-light small-text text-gray">1 Minutes ago</span>
                                </h6>
                                <p class="font-weight-light small-text mb-0">
                                    The meeting is cancelled
                                </p>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                                <img src="https://www.placehold.it/36x36" alt="image" class="profile-pic">
                            </div>
                            <div class="preview-item-content flex-grow">
                                <h6 class="preview-subject ellipsis font-weight-normal text-dark mb-1">Tim Cook
                                    <span class="float-right font-weight-light small-text text-gray">15 Minutes ago</span>
                                </h6>
                                <p class="font-weight-light small-text mb-0">
                                    New product launch
                                </p>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                                <img src="https://www.placehold.it/36x36" alt="image" class="profile-pic">
                            </div>
                            <div class="preview-item-content flex-grow">
                                <h6 class="preview-subject ellipsis font-weight-normal text-dark mb-1"> Johnson
                                    <span class="float-right font-weight-light small-text text-gray">18 Minutes ago</span>
                                </h6>
                                <p class="font-weight-light small-text mb-0">
                                    Upcoming board meeting
                                </p>
                            </div>
                        </a>
                    </div>
                </li>--}}
                <li class="nav-item dropdown">
                    {{--<a title="Vous avez {{$nbnotifs}} notification{{$nbnotifs>1 ?'s' :'' }}" class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="{{route('alerts_path')}}">
                        @if($nbnotifs>0)
                            <i class="mdi mdi-bell-outline"></i>
                            <span class="count bg-danger">{{$nbnotifs}}</span>
                        @else
                            <i class="mdi mdi-bell-outline"></i>
                        @endif
                    </a>--}}
                    {{--<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                        <a class="dropdown-item py-3">
                            <p class="mb-0 font-weight-medium float-left">You have 4 new notifications
                            </p>
                            <span class="badge badge-pill badge-inverse-info float-right">View all</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                                <div class="preview-icon bg-inverse-success">
                                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                                </div>
                            </div>
                            <div class="preview-item-content">
                                <h6 class="preview-subject font-weight-normal text-dark mb-1">Application Error</h6>
                                <p class="font-weight-light small-text mb-0">
                                    Just now
                                </p>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                                <div class="preview-icon bg-inverse-warning">
                                    <i class="mdi mdi-comment-text-outline mx-0"></i>
                                </div>
                            </div>
                            <div class="preview-item-content">
                                <h6 class="preview-subject font-weight-normal text-dark mb-1">Settings</h6>
                                <p class="font-weight-light small-text mb-0">
                                    Private message
                                </p>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                                <div class="preview-icon bg-inverse-info">
                                    <i class="mdi mdi-email-outline mx-0"></i>
                                </div>
                            </div>
                            <div class="preview-item-content">
                                <h6 class="preview-subject font-weight-normal text-dark mb-1">New user registration</h6>
                                <p class="font-weight-light small-text mb-0">
                                    2 days ago
                                </p>
                            </div>
                        </a>
                    </div>--}}
                </li>
                <li class="nav-item mr-0">
                    <a href="{{route('profiluserclt')}}" class="nav-link py-0 pr-0">
                        <span class="text-black d-none d-lg-inline-block text-white mr-2">Bonjour, {{Auth::user()->name}} !</span>
                        <img class="img-xs rounded-circle" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}" alt="profile image">
                    </a>
                </li>
            </ul>
            <button type="button" class="navbar-toggler d-block d-md-none">
                <i class="mdi mdi-menu"></i>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="nav-bottom">
            <ul class="navbar-nav">
                <li class="nav-item dropdown {{set_active('homeclt')}}">
                    <a class="nav-link count-indicator dropdown-toggle" id="dashboard-dropdown" href="{{route('homeclt')}}">
                        Accueil
                    </a>
                </li>
               {{-- <li class="nav-item dropdown {{set_active('config_path')}} {{set_active('alerts_path')}}">
                    <a class="nav-link count-indicator dropdown-toggle" id="finance-dropdown" href="#" data-toggle="dropdown">
                        Confirguration
                    </a>
                    <div class="dropdown-menu dropdown-left navbar-dropdown" aria-labelledby="finance-dropdown">
                        <ul>
                            <li class="dropdown-item {{set_active('config_path')}}">
                                <a href="{{route('config_path')}}" class="dropdown-link">Ma confirguration</a>
                            </li>
                            <li class="dropdown-item {{set_active('alerts_path')}}">
                                <a href="{{route('alerts_path')}}" class="dropdown-link">Alerte</a>
                            </li>
                        </ul>
                    </div>
                </li>--}}
                <li class="nav-item dropdown {{set_active('export_path')}}">
                    <a class="nav-link count-indicator dropdown-toggle" id="project-dropdown" href="{{route('export_path')}}">
                        Export
                    </a>
                </li>

                <li class="nav-item dropdown {{set_active('profiluserclt')}}">
                    <a class="nav-link count-indicator dropdown-toggle" id="project-dropdown" href="{{route('profiluserclt')}}">
                        Mon Compte
                    </a>
                </li>

                {{--<li class="nav-item dropdown">
                    <a class="nav-link count-indicator dropdown-toggle" id="revenue-dropdown" href="#" data-toggle="dropdown">
                        Revenue Report
                    </a>
                    <div class="dropdown-menu dropdown-left navbar-dropdown" aria-labelledby="revenue-dropdown">
                        <ul>
                            <li class="dropdown-item">
                                <a href="#" class="dropdown-link">Menu Link 1</a>
                            </li>
                            <li class="dropdown-item">
                                <a href="#" class="dropdown-link">Menu Link 2</a>
                            </li>
                            <li class="dropdown-item">
                                <a href="#" class="dropdown-link">Menu Link 3</a>
                            </li>
                            <li class="dropdown-item">
                                <a href="#" class="dropdown-link">Menu Link 4</a>
                            </li>
                            <li class="dropdown-item">
                                <a href="#" class="dropdown-link">Menu Link 5</a>
                            </li>
                        </ul>
                    </div>
                </li>--}}
            </ul>
            <ul class="navbar-nav ml-auto d-none d-lg-block">
                <li class="nav-item dropdown">
                    <a class="nav-link count-indicator dropdown-toggle" id="project-dropdown" data-toggle="dropdown"  href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        Déconnexion
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>