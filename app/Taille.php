<?php

namespace xp;

use Illuminate\Database\Eloquent\Model;

class Taille extends Model
{
    public function pinsTaille(){
        return $this->hasMany('xp\Pin');
    }
}
