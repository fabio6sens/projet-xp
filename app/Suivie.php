<?php

namespace xp;

use Illuminate\Database\Eloquent\Model;

class Suivie extends Model
{
    public $timestamps = false;
    protected $table = 'annonceur_user';
}
