<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 16/08/2018
 * Time: 15:37
 */

//**************************************************
//*** GESTION DES ACTIVES (LES LIENS DU NAV)********
//**************************************************
if(!function_exists('set_active')){
    function set_active($file){
        //dd($file);
        return Route::is($file) ? 'active' : '';
    }
}