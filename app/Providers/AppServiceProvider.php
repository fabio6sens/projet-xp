<?php

namespace xp\Providers;

use Illuminate\Support\ServiceProvider;
use xp\Notification;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $listLayout = array(
            'includes.nav',
            'pathmaster.includes.nav',
        );
        view()->composer($listLayout,function ($view){
            $view->with('nbnotifs',Notification::mynotifs());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
