<?php

namespace xp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Annonceur extends Model
{
    public function pinsAnnonceur(){
        return $this->hasMany('xp\Pin');
    }

    public function userAnnonceur(){
        return $this->belongsToMany('xp\User', 'annonceur_user', 'annonceur_id', 'user_id');
    }

    public function allAnnonceurByUser(){
        return $this->userAnnonceur()->where('user_id',Auth::user()->id);
    }
}
