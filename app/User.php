<?php

namespace xp;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','confirmation_token','slug','typecompte','etat',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function annonceurUser(){
        return $this->belongsToMany('xp\Annonceur', 'annonceur_user', 'user_id', 'annonceur_id');
    }

    public function userAnnonceurs(){
        return $this->annonceurUser()->where('user_id',Auth::user()->id);
    }
}
