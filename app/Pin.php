<?php

namespace xp;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    public function annonceurPins(){
        return $this->belongsTo('xp\Annonceur','annonceur_id');
    }

    public function taillePins(){
        return $this->belongsTo('xp\Taille','taille_id');
    }

    public function userPins(){
        return $this->belongsTo('xp\User','user_id');
    }
}
