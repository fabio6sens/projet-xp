<?php

namespace xp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{
    public static function mynotifs(){
        return static::where([
            ['user_id',Auth::user()->id],
            ['read_at','0']
        ])->count();
    }
}
