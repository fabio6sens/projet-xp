<?php

namespace xp\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->typecompte == 1) {
            return redirect('/bienvenue');
        }
        return $next($request);
    }
}
