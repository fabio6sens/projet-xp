<?php

namespace xp\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AnnonceurMiddle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->typecompte);
        if (Auth::user()->typecompte != 1) {
            return redirect('/home');
        }
        return $next($request);
    }
}
