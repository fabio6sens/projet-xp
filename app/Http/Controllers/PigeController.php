<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use xp\Annonceur;
use xp\Gain;
use xp\Notification;
use xp\Pin;
use xp\Taille;

class PigeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($slug)
    {
        \Carbon\Carbon::setLocale('fr');
        $pins = Pin::with('annonceurPins')->with('taillePins')->where('slug',$slug)->first();
        $double = Pin::where([['lat',$pins->lat],['lng',$pins->lng],['id','!=',$pins->id]])->first();
        return view('piges/profil',compact('pins','double'));
    }

    public function indexclt($slug)
    {
        \Carbon\Carbon::setLocale('fr');
        $pins = Pin::with('annonceurPins')->with('taillePins')->where('slug',$slug)->first();
        $double = Pin::where([['lat',$pins->lat],['lng',$pins->lng],['id','!=',$pins->id]])->first();
        return view('pathmaster/piges/profil',compact('pins','double'));
    }

    public function all()
    {
        \Carbon\Carbon::setLocale('fr');

        //$annonceurs = Annonceur::where('etat','1')->orderBy('libelle','asc')->get();

        $pins = Pin::with('annonceurPins')->with('taillePins')->with('userPins')->orderBy('updated_at','desc')->get();
        if(Auth::user()->typecompte==2){
            $pins = Pin::with('annonceurPins')->with('taillePins')->with('userPins')->where('user_id',Auth::user()->id)->orderBy('updated_at','desc')->get();
        }

        return view('piges/index',compact('pins'));
    }

    public function allValid()
    {
        \Carbon\Carbon::setLocale('fr');
        $pins = Pin::with('annonceurPins')->with('taillePins')->with('userPins')->where('actif','0')->orderBy('updated_at','desc')->get();
        return view('piges/index-valid',compact('pins'));
    }

    public function valid($slug){
        //dd($slug);

        //TABLE DE NOTIFICATION
        $pin = Pin::where('slug',$slug)->firstOrFail();
        $idpins = $pin->slug;
        $pigisteID = $pin->user_id;
        $idannonceur = $pin->annonceur_id;
        $pin->actif = '1';
        $pin->save();
        $annon = Annonceur::find($idannonceur);
        $iduser = $annon->userAnnonceur()->pluck('user_id')->toArray();

        $portefeuille = new Gain();
        $portefeuille->user_id = $pigisteID;
        $portefeuille->pin_id = $idpins;
        $portefeuille->save();

        foreach ($iduser as $id){
            $notificate = new Notification();
            $notificate->annonceur_id = $idannonceur;
            $notificate->user_id = $id;
            $notificate->pin_id = $idpins;
            $notificate->save();
        }

        return redirect()->route('pins_path_valid')->with('success','✔ Félicitation ! vous venez de valider une pige');
    }

    public function rejet(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);

        $valider = Validator::make($request->all(),[
            'motifrejet' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('pins_path_valid')->withErrors($valider->errors());
        }else{
            $motifrejet = $reponse['motifrejet'];
            $ipop = $reponse['ipop'];

            $pin = Pin::where('slug',$ipop)->firstOrFail();
            $annoncerID = $pin->annonceur_id;
            $userID = $pin->user_id;
            $pin->actif = '2';
            $pin->save();

            $notificate = new Notification();
            $notificate->annonceur_id = $annoncerID;
            $notificate->user_id = $userID;
            $notificate->pin_id = $ipop;
            $notificate->type = 'REJET';
            $notificate->data = $motifrejet;
            $notificate->save();
        }

        return redirect()->route('pins_path_valid')->with('success','✔ Félicitation ! vous venez de rejeter une pige');
    }

    public function add()
    {
        //\Carbon\Carbon::setLocale('fr');
        $annonceurs = Annonceur::where('etat','1')->orderBy('libelle','asc')->get();
        $tailles = Taille::orderBy('taille','asc')->get();
        return view('piges/add',compact('annonceurs','tailles'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $file = $request->file('file');

        $valider = Validator::make($request->all(),[

            'type' =>'required',
            'taille' =>'required',
            'annonceur' =>'required',
            'lat' =>'required',
            'lng' =>'required',
            /*'etat' =>'required',
            'campagne' =>'required',
            'activ' =>'required',
            'marque' =>'required',
            'agence' =>'required',
            'regie' =>'required',
            'note' =>'required',*/

        ]);

        if($valider->fails()){
            return redirect()->route('pins_path')->withErrors($valider->errors());
        }else{
            if($request->hasFile('file')){
                $date=new \DateTime('now');
                $newname= $date->format('dmYHis');
                //try{
                    $pige = new Pin();

                    $pige->slug= str_slug(str_random(8));
                    $pige->type= $reponse['type'];
                    $pige->taille_id= $reponse['taille'];
                    $pige->annonceur_id= $reponse['annonceur'];
                    $pige->lat= $reponse['lat'];
                    $pige->lng= $reponse['lng'];
                    /*$pige->etat= $reponse['etat'];
                    $pige->sectActiv= $reponse['activ'];
                    $pige->marque= $reponse['marque'];
                    $pige->agence= $reponse['agence'];
                    $pige->regie= $reponse['regie'];
                    $pige->note= $reponse['note'];
                    $pige->campagne= $reponse['campagne'];
                    $pige->eclaire= $reponse['eclaire'];
                    $pige->achat= $reponse['achat'];
                    $pige->vente= $reponse['vente'];
                    $pige->odm= $reponse['odm'];
                    $pige->tm= $reponse['tm'];*/

                    $file = $request->file('file');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $folderName = '../public_html/piges/panneau';
                    $picture = str_random(8).$newname.'.'. $extension;

                    if($file->getClientSize() > 1000000 ){
                        $img = Image::make($file->getRealPath());
                        $img->resize(500,500)->save($folderName.'/'.$picture);
                    }else{
                        $file->move($folderName,$picture);
                    }

                    $pige->panneau = $picture;
                    $pige->user_id= Auth::user()->id;
                    $pige->save();

                    if($reponse['type'] == 'panneaux double'){
                        return redirect()->route('pins_double',$pige->id);
                    }else{
                        return redirect()->route('pins_path')->with('success','✔ Félicitation ! vous venez d\' ajoute une pige');
                    }

                /*}catch (\Exception $e){
                    //dd($e->getMessage());
                }*/

            }else{
                return redirect()->route('pins_path')->with('error','Veuillez inserez la Photo du panneau');
            }

        }
        //dd($reponse);
    }

    public function edit($slug){
        $annonceurs = Annonceur::where('etat','1')->orderBy('libelle','asc')->get();
        $tailles = Taille::orderBy('taille','asc')->get();
        $pin=Pin::where('slug',$slug)->firstOrFail();
        return view('piges/edit',compact('pin','annonceurs','tailles'));
    }

    public function update($slug,Request $request){
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'campagne' =>'required',
            'type' =>'required',
            'taille' =>'required',
            'etat' =>'required',
            'annonceur' =>'required',
            'activ' =>'required',
            'marque' =>'required',
            'agence' =>'required',
            'regie' =>'required',
            'note' =>'required',
            'lat' =>'required',
            'lng' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('pins_path')->withErrors($valider->errors());
        }else{

            $pin = Pin::where('slug',$slug)->firstOrFail();

            if($request->hasFile('file')){
                $date=new \DateTime('now');
                $newname= $date->format('dmYHis');

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '../public_html/piges/panneau';
                $picture = str_random(8).$newname.'.'. $extension;

                if (!empty($pin->panneau)) {
                    unlink($folderName.'/'.$pin->panneau);
                }

                if($file->getClientSize() > 1000000 ){
                    $img = Image::make($file->getRealPath());
                    $img->resize(500,500)->save($folderName.'/'.$picture);
                }else{
                    $file->move($folderName,$picture);
                }

                $pin->panneau = $picture;
                //$file->move($folderName,$picture);

            }

            $pin->type= $reponse['type'];
            $pin->taille_id= $reponse['taille'];
            $pin->etat= $reponse['etat'];
            $pin->annonceur_id= $reponse['annonceur'];
            $pin->sectActiv= $reponse['activ'];
            $pin->marque= $reponse['marque'];
            $pin->agence= $reponse['agence'];
            $pin->regie= $reponse['regie'];
            $pin->note= $reponse['note'];

            $pin->campagne= $reponse['campagne'];
            $pin->eclaire= $reponse['eclaire'];
            $pin->achat= $reponse['achat'];
            $pin->vente= $reponse['vente'];
            $pin->odm= $reponse['odm'];
            $pin->tm= $reponse['tm'];

            $pin->actif= '0';
            //$pin->lat= $reponse['lat'];
            //$pin->lng= $reponse['lng'];
            $pin->user_id= Auth::user()->id;
            $pin->save();

            /*$idpins = $pin->slug;
            $idannonceur = $pin->annonceur_id;
            $annon = Annonceur::find($idannonceur);
            $iduser = $annon->userAnnonceur()->pluck('user_id')->toArray();

            foreach ($iduser as $id){
                $notificate = new Notification();
                $notificate->annonceur_id = $idannonceur;
                $notificate->user_id = $id;
                $notificate->pin_id = $idpins;
                $notificate->save();
            }*/
            //dd($iduser);

            return redirect()->route('pins_path')->with('success','✔ Félicitation ! vous venez de modifier une pige');

            //dd($reponse);
        }
    }

    public function pinsdouble($id){
        $annonceurs = Annonceur::where('etat','1')->orderBy('libelle','asc')->get();

        $idpins = Pin::find($id);
        $taille = $idpins->taille_id;
        $latitude = $idpins->lat;
        $longitude = $idpins->lng;
        //dd($latitude);

        return view('piges/addouble',compact('id','annonceurs','taille','latitude','longitude'));
    }

    public function storedouble(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $file = $request->file('file');

        $valider = Validator::make($request->all(),[

            'taille' =>'required',
            'annonceur' =>'required',
            'lat' =>'required',
            'lng' =>'required',
            /*'etat' =>'required',
            'campagne' =>'required',
            'activ' =>'required',
            'marque' =>'required',
            'agence' =>'required',
            'regie' =>'required',
            'note' =>'required',*/

        ]);

        if($valider->fails()){
            return redirect()->route('pins_double',$reponse['id'])->withErrors($valider->errors());
        }else{
            if($request->hasFile('file')){
                $date=new \DateTime('now');
                $newname= $date->format('dmYHis');
                //try{
                $pige = new Pin();

                $pige->slug= str_slug(str_random(8));
                $pige->type= $reponse['type'];
                $pige->taille_id= $reponse['taille'];
                $pige->annonceur_id= $reponse['annonceur'];
                $pige->lat= $reponse['lat'];
                $pige->lng= $reponse['lng'];
                /*$pige->etat= $reponse['etat'];
                $pige->sectActiv= $reponse['activ'];
                $pige->marque= $reponse['marque'];
                $pige->agence= $reponse['agence'];
                $pige->regie= $reponse['regie'];
                $pige->note= $reponse['note'];
                $pige->campagne= $reponse['campagne'];
                $pige->eclaire= $reponse['eclaire'];
                $pige->achat= $reponse['achat'];
                $pige->vente= $reponse['vente'];
                $pige->odm= $reponse['odm'];
                $pige->tm= $reponse['tm'];*/

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '../public_html/piges/panneau';
                $picture = str_random(8).$newname.'.'. $extension;

                if($file->getClientSize() > 1000000 ){
                    $img = Image::make($file->getRealPath());
                    $img->resize(500,500)->save($folderName.'/'.$picture);
                }else{
                    $file->move($folderName,$picture);
                }

                //$file->move($folderName,$picture);

                $pige->panneau = $picture;
                $pige->user_id= Auth::user()->id;
                $pige->save();

                return redirect()->route('pins_path')->with('success','✔ Félicitation ! vous venez d\' ajoute une pige');

                /*}catch (\Exception $e){
                    //dd($e->getMessage());
                }*/

            }else{
                return redirect()->route('pins_double',$reponse['id'])->with('error','Veuillez inserez la Photo du panneau');
            }

        }
        //dd($reponse);
    }
}
