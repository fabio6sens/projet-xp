<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use xp\User;

class ProfilCltController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pathmaster/profil/index');
    }

    public function update(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'nom' =>'max:255|min:3|required',
            //'prenoms' =>'max:255|min:3|required',
            //'sexe' =>'max:255|min:3|required',
            'phone' =>'required|string',
            'address' =>'required',
            //'bio' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('profiluserclt')->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $reponse['nom'];
            //$user->prenoms = $reponse['prenoms'];
            //$user->sexe = $reponse['sexe'];
            $user->contact = $reponse['phone'];
            $user->address = $reponse['address'];
            //$user->bio = $reponse['bio'];
            $user->save();
            //dd($reponse);
            return redirect()->route('profiluserclt')->with('success','✔ Félicitation ! modification a été modifié');
        }
    }

    public function updatepass(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->route('profiluserclt')->withErrors($valider->errors());
        }else{
            $oldpass = $reponse['motpass'];
            $newpass = $reponse['newpass'];
            $newpassconfirm = $reponse['confirmpass'];
            $passUser = Auth::user()->password;

            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){
                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();
                    return redirect()->route('profiluserclt')->with('success','Votre mot de passe a été mise a jour');
                }else{
                    return redirect()->route('profiluserclt')->with('error','Désolé votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->route('profiluserclt')->with('error','Les mots de passe sont différents');
            }
        }

    }

    public function avatar(Request $request){
        //dd($request->file('fileUser'));
        $file=$request->file('fileUser');
        $fileName=$file->getClientOriginalName();
        $fileextensions=$file->getClientOriginalExtension();
        $fileSize=$file->getClientSize();
        //dd($file);

        try{
            if($fileSize <= 3000263):
                $aleatoire=str_random(8);
                $uploadName = htmlspecialchars(trim(strtolower($fileName)));
                $upload_nom =$aleatoire . '.' . $fileextensions;
                $upload_dest= '../public_html/userAvatar/';
                $fileextension=strtolower($fileextensions);
                $extensions_autorisees=array('jpeg','jpg','gif','tiff','bmp','png');

                if(in_array($fileextension ,$extensions_autorisees)):

                    if (!empty(Auth::user()->img)) {
                        unlink($upload_dest.Auth::user()->img);
                    }
                    $users= User::find(Auth::user()->id);
                    $users->img= $upload_nom;
                    $users->save();

                    $file->move($upload_dest,$upload_nom);

                    return redirect()->route('profiluserclt')->with('success','Félicitation ! votre avatar a été mise à jours');
                else:
                    return redirect()->route('profiluserclt')->with('error','Désolé ! L\'extension de l\'image n\'est pas autorise');
                endif;
            else:
                return redirect()->route('profiluserclt')->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
            endif;

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
}
