<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use xp\Client;
use xp\Pin;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(Request $request){

        $itemClt = Pin::get();

        $itemClt->map(function($item){
            if (Auth::user()->typecompte==1){
                $item->profile_url = route('pin_profil', $item->slug);
            }else{
                $item->profile_url = route('profil_path', $item->slug);
            }
        });

    	return $itemClt;
    }

    public function searchItem(Request $request){
        //$requete = $request->all();
        $motcle = $request->cle;
        $searchValue = explode(' ',$motcle);

        $itemClt = Client::select('*');
        foreach ($searchValue as $search) {
            $trim = trim(htmlentities($search));
            if (!empty($trim)){
                $itemClt->orWhere('name', 'LIKE', '%'. $search .'%')->orWhere('marque', 'LIKE', '%'. $search .'%')->orWhere('regie', 'LIKE', '%'. $search .'%');
            }
        }
        $searchResults= $itemClt->get();
        //$itemClt = Client::where('name','LIKE','%'.$motcle.'%')->get();
        //$itemClt = Client::where('name',$motcle)->get();
        return $searchResults;
    }
}
