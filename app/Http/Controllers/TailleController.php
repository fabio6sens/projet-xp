<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use xp\Taille;

class TailleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tailles = Taille::get();
        return view('size/index',compact('tailles'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('size_path')->withErrors($valider->errors());
        }else{
            if($request->hasFile('file')){

                $annonceur = new Taille();
                $annonceur->taille = $reponse['nom'];

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '../public_html/piges/marqueurs';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $annonceur->marqueur = $picture;

                $annonceur->save();

                return redirect()->route('size_path')->with('success','✔ Félicitation ! la taille a été ajouté');

            }else{
                return redirect()->route('size_path')->with('error','Veuillez inserez la Photo du marqueur');
            }
        }
        //dd($reponse);
    }

    public function itemsiza(Request $request)
    {
        $response=$request->all();
        $id=$response['id'];

        $taille=Taille::findOrFail($id);
        if($taille):
            $data[]=array (
                "id"  => $taille->id,
                "libelle"  => $taille->taille,
                "marqueur"  => $taille->marqueur,
            );
            return json_encode(array('data'=>$data));
        else:
            return json_encode(array('data'=>0));
        endif;
    }

    public function update(Request $request){
        $reponse = $request->except(['_token']);
        //dd($request->file('file'));
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'slug' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('size_path')->withErrors($valider->errors());
        }else{

            $taille = Taille::findOrFail($reponse['slug']);
            $taille->taille = $reponse['libelle'];

            if($request->hasFile('file')){
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '../public_html/piges/marqueurs';
                $picture = str_random(8).'.'. $extension;
                //dd($picture);
                if (!empty($taille->marqueur)) {
                    unlink($folderName.'/'.$taille->marqueur);
                }
                $taille->marqueur = $picture;
                $file->move($folderName,$picture);
            }

            $taille->save();

            return redirect()->route('size_path')->with('success','✔ Félicitation ! la taille a été modifié');
        }
    }
}
