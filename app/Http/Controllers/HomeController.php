<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use xp\Annonceur;
use xp\Pin;
use JavaScript;
use xp\Taille;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPins = Pin::with('annonceurPins')->with('taillePins')->where('actif','1')->get();

        $allPins->map(function($item){
            $item->profile_url = route('profil_path', $item->slug);
        });
        //dd($allPins[1]->profile_url);
        JavaScript::put(compact('allPins'));

        $annonceurs = Annonceur::where('etat','1')->orderBy('libelle','asc')->get();
        $tailles = Taille::orderBy('taille','asc')->get();

        return view('home',compact('annonceurs','tailles'));
    }

    public function indexclt()
    {
        $allPins = Pin::with('annonceurPins')->with('taillePins')->where('actif','1')->get();

        $allPins->map(function($item){
            $item->profile_url = route('pin_profil', $item->slug);
        });

        JavaScript::put(compact('allPins'));

        $annonceurs = Annonceur::where('etat','1')->orderBy('libelle','asc')->get();
        $tailles = Taille::orderBy('taille','asc')->get();

        return view('pathmaster.home',compact('annonceurs','tailles'));
    }

    public function searchForClt(Request $request){
        $reponse=$request->all();
        //dd($reponse);
        $motcle = $reponse['valueSearch'];
        $searchValue = explode(' ',$motcle);

        $itemClt=DB::table('pins')
            ->join('annonceurs','pins.annonceur_id', '=', 'annonceurs.id')
            ->join('tailles','pins.taille_id', '=', 'tailles.id')
            ->select('annonceurs.libelle','tailles.marqueur','tailles.taille','pins.*')
            ->where('actif','1');
        //dd($itemClt);

        //$itemClt = Pin::with('annonceurPins')->select('*');
        foreach ($searchValue as $search) {
            $trim = trim(htmlentities($search));
            if (!empty($trim)){
                $itemClt->orWhere('libelle', 'LIKE', '%'. $search .'%')->orWhere('taille', 'LIKE', '%'. $search .'%')->orWhere('marque', 'LIKE', '%'. $search .'%')->orWhere('regie', 'LIKE', '%'. $search .'%');
            }
        }
        $searchResults= $itemClt->get();
        //dd($searchValue,$searchResults);
        if(count($searchResults)>0):
            /* foreach($searchResults as $file):
                 $fileAndUser[]=[
                     "id"=>$file->id,
                     "libelle"=>$file->libelle,
                     "marqueur"=>$file->marqueur,
                     "type"=>$file->type,
                     "taille"=>$file->taille,
                     "etat"=>$file->etat,
                     "sectActiv" => $file->sectActiv,
                     "marque"=>$file->marque,
                     "agence"=>$file->agence,
                     "regie"=>$file->regie,
                     'lat'=>$file->lat,
                     'lng'=>$file->lng,
                     'panneau'=>$file->panneau,
                     "updated_at"=>$file->updated_at,
                     "profile_url" => route('pin_profil', $file->slug),
                     //'user_id'=>$file->user_id,
                     //"created_at"=>$file->created_at,
                     //"note"=>$file->note,
                     //"annonceur_id"=>$file->annonceur_id,
                     //"slug"=>$file->slug,
                 ];
             endforeach;*/
            $searchResults->map(function($item){
                $item->profile_url = route('pin_profil', $item->slug);
            });

            return json_encode(array('data'=>$searchResults));
        else:
            return json_encode(array('data'=>0));
        endif;
        /*dd($searchResults);
        return $searchResults;*/
    }


    public function searchAd(Request $request){
        $reponse=$request->all();
        //dd($reponse);
        $motcle = $reponse['valueSearch'];
        $searchValue = explode(' ',$motcle);
        //dd($searchValue);
        $itemClt=DB::table('pins')
            ->join('annonceurs','pins.annonceur_id', '=', 'annonceurs.id')
            ->join('tailles','pins.taille_id', '=', 'tailles.id')
            ->select('annonceurs.libelle','tailles.marqueur','tailles.taille','pins.*')
            ->where('actif','1');
        //dd($itemClt);

        //$itemClt = Pin::with('annonceurPins')->select('*');
        foreach ($searchValue as $search) {
            $trim = trim(htmlentities($search));
            //dd($trim);
            if (!empty($trim)){
                $itemClt->orWhere('libelle', 'LIKE', '%'. $search .'%')->orWhere('taille', 'LIKE', '%'. $search .'%')->orWhere('marque', 'LIKE', '%'. $search .'%')->orWhere('regie', 'LIKE', '%'. $search .'%');
            }
        }
        $searchResults= $itemClt->get();
        //dd($searchResults);
        if(count($searchResults)>0):
            /*foreach($searchResults as $file):
                $fileAndUser[]=[
                    "id"=>$file->id,
                    "libelle"=>$file->libelle,
                    "marqueur"=>$file->marqueur,
                    "type"=>$file->type,
                    "taille"=>$file->taille,
                    "etat"=>$file->etat,
                    "sectActiv" => $file->sectActiv,
                    "marque"=>$file->marque,
                    "agence"=>$file->agence,
                    "regie"=>$file->regie,
                    'lat'=>$file->lat,
                    'lng'=>$file->lng,
                    'panneau'=>$file->panneau,
                    "updated_at"=>$file->updated_at,
                    "profile_url" => route('profil_path', $file->slug),
                    //"note"=>$file->note,
                    //"annonceur_id"=>$file->annonceur_id,
                    ////"slug"=>$file->slug,
                    //'user_id'=>$file->user_id,
                    //"created_at"=>$file->created_at,
                ];
            endforeach;*/
            $searchResults->map(function($item){
                $item->profile_url = route('profil_path', $item->slug);
            });
            return json_encode(array('data'=>$searchResults));
        else:
            return json_encode(array('data'=>0));
        endif;
        /*dd($searchResults);
        return $searchResults;*/
    }

    public function searchmja(Request $request){
        //dd($request->all());
        $response=$request->all();
        $lat=$response['lat'];
        $lng=$response['lon'];

        $location = DB::table('pins')
            ->join('annonceurs','pins.annonceur_id', '=', 'annonceurs.id')
            ->join('tailles','pins.taille_id', '=', 'tailles.id')
            ->select('annonceurs.libelle','tailles.marqueur','tailles.taille','pins.*', DB::raw(sprintf('(6371 * acos(cos(radians(%1$.7f)) * cos(radians(lat)) * cos(radians(lng) - radians(%2$.7f)) + sin(radians(%1$.7f)) * sin(radians(lat)))) AS distance', $lat, $lng)))
            ->where('pins.actif','1')
            ->where('pins.updated_at','<=',DB::raw('DATE_SUB(CURDATE(), INTERVAL 30 DAY)'))
            ->having('distance', '<=', 10)
            //->orderBy('distance', 'asc')
            ->get();
        $location->map(function($item){
            $item->profile_url = route('profil_path', $item->slug);
            $item->edit_url = route('pins_edit_path', $item->slug);
        });

        return json_encode(array('data'=>$location));
        //dd($location);
    }

    public function searchmini(Request $request){
        $reponse=$request->all();
        //dd($reponse);
        $itemClt=DB::table('pins')
            ->join('annonceurs','pins.annonceur_id', '=', 'annonceurs.id')
            ->join('tailles','pins.taille_id', '=', 'tailles.id')
            ->select('annonceurs.libelle','tailles.marqueur','tailles.taille','pins.*');

        if(isset($reponse['campagne']) && !empty($reponse['campagne']))
        {$itemClt->where('campagne', 'LIKE', '%'.$reponse['campagne'].'%');}

        if(isset($reponse['eclaire']) && !empty($reponse['eclaire']))
        {$itemClt->where('eclaire', 'LIKE', '%'.$reponse['eclaire'].'%');}

        if(isset($reponse['agence']) && !empty($reponse['agence']))
        {$itemClt->where('agence', 'LIKE', '%'.$reponse['agence'].'%');}

        if(isset($reponse['marque']) && !empty($reponse['marque']))
        {$itemClt->where('marque', 'LIKE', '%'.$reponse['marque'].'%');}

        if(isset($reponse['regie']) && !empty($reponse['regie']))
        {$itemClt->where('regie', 'LIKE', '%'.$reponse['regie'].'%');}

        if(isset($reponse['activ']) && !empty($reponse['activ']))
        {$itemClt->where('sectActiv', 'LIKE', '%'.$reponse['activ'].'%');}

        if(isset($reponse['type']) && !empty($reponse['type']))
        {$itemClt->where('type', 'LIKE', '%'.$reponse['type'].'%');}

        if(isset($reponse['taille']) && !empty($reponse['taille']) && $reponse['taille'] != 'Taille')
        {$itemClt->where('taille', 'LIKE', '%'.$reponse['taille'].'%');}

        if(isset($reponse['etat']) && !empty($reponse['etat']))
        {$itemClt->where('pins.etat', 'LIKE', '%'.$reponse['etat'].'%');}

        if(isset($reponse['annonceur']) && !empty($reponse['annonceur']) && $reponse['annonceur'] != 'Annonceur')
        {$itemClt->where('libelle', 'LIKE', '%'.$reponse['annonceur'].'%');}

        $searchResults= $itemClt->where('actif','1')->get();

        if(count($searchResults)>0):
            $searchResults->map(function($item){
                $item->profile_url = route('profil_path', $item->slug);
            });
            return json_encode(array('data'=>$searchResults));
        else:
            return json_encode(array('data'=>0));
        endif;
        /*dd($searchResults);
        return $searchResults;*/
    }
    
}
