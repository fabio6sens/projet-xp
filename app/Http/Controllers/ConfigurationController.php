<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use xp\Annonceur;
use xp\Suivie;
use xp\User;

class ConfigurationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $user = User::findOrFail(Auth::user()->id);
        $idannonce  = $user->annonceurUser()->pluck('annonceur_id')->toArray();
        $annonceursNotUser= Annonceur::whereNotIn('id',$idannonce)->where('etat','1')->get();

        $annonceurs=DB::table('annonceur_user')
            ->join('annonceurs','annonceur_user.annonceur_id', '=', 'annonceurs.id')
            ->join('users','annonceur_user.user_id', '=', 'users.id')
            ->where('annonceur_user.user_id',Auth::user()->id)
            ->select('users.*','annonceurs.*')
            ->paginate(20);
        //dd($var);

        return view('pathmaster/config/index',compact('annonceurs','annonceursNotUser'));
    }

    public function add(Request $request){
        $reponse = $request->except(['_token']);

        $annonceur = $reponse['ann'];

        if(count($annonceur)>0){
            //$user = User::findOrFail(Auth::user()->id);
            //$user->annonceurUser()->sync($annonceur);

            foreach($annonceur as $data):
                $suivie = new Suivie();
                $suivie->annonceur_id = $data;
                $suivie->user_id = Auth::user()->id;

                $seach = DB::table('annonceur_user')->where('annonceur_id',$data)
                    ->where('user_id',Auth::user()->id)
                    ->count();

                if($seach==0){
                    $suivie->save();
                }
            endforeach;

            return redirect()->route('config_path')->with('success','Félicitation vous venez de vous abonnés à des annonceurs');
        }else{
            return redirect()->route('config_path')->with('error','Désolé veuillez selectionnez des annonceurs');
        }

        //$ann = Annonceur::findOrFail($id);
        //$libelle = $ann->libelle;
        /*$ann = Annonceur::findOrFail($id);
        $allann = $ann->userAnnonceur;
        dd($allann);*/

        /*$user = User::findOrFail(Auth::user()->id);
        $var  = $user->annonceurUser()->pluck('libelle','annonceur_id')->toArray();
        dd($var);*/

        /*$user = User::findOrFail(Auth::user()->id);
        foreach ($user->annonceurUser as $role) {
            dump($role->libelle);
        }*/
    }

    public function del($id,$libelle){

        if($id and $libelle){
            $user = User::findOrFail(Auth::user()->id);
            $user->annonceurUser()->detach($id);

            return redirect()->route('config_path')->with('success','Vous venez de vous désabonnés à l\'annonceur '.$libelle.'');
        }else{
            dd($id);
        }
    }


}
