<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use xp\Pin;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $allPins = Pin::with('annonceurPins')->with('taillePins')->where('actif','1')->get();

        $allPins->map(function($item){
            $item->pathPanneau = url('piges/panneau/'.$item->panneau);
        });
        //$allPins->toArray();

        //dd($allPins[9]->taillePins->taille);
        $customerArray[] = array('NUM','TYPE','TAILLE','ETAT','SECTEUR D\'ACTIVITE','ANNONCEUR','MARQUE','AGENCE','REGIE','PANNEAU','DATE DE MISE A JOUR');
        $i=1;
        foreach ($allPins as $pin){
            $customerArray[]= array(
                'NUM'=>$i,
                'TYPE'=> $pin->type,
                'TAILLE'=> $pin->taillePins->taille,
                'ETAT'=> $pin->etat,
                'SECTEUR D\'ACTIVITE'=> $pin->sectActiv,
                'ANNONCEUR'=> $pin->annonceurPins->libelle,
                'MARQUE'=> $pin->marque,
                'AGENCE'=> $pin->agence,
                'REGIE'=> $pin->regie,
                'PANNEAU'=> $pin->pathPanneau,
                'DATE DE MISE A JOUR'=> $pin->updated_at->format('d-m-Y')
            );
            $i++;
        }
        $date=new \DateTime('now');
        $date = $date->format('d-m-Y');

        Excel::create("Fabien-".$date,function($excel) use($customerArray,$date){
            $excel->setTitle("Fabien-".$date);
            $excel->sheet("Fabien-".$date,function ($sheet) use ($customerArray){
                $sheet->fromArray($customerArray,null,'A1',false,false);
            });

        })->download('xlsx');
    }
}
