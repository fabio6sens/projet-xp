<?php

namespace xp\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use xp\Gain;
use xp\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::where('typecompte','!=',3)->orderBy('id','desc')->get();
        return view('users/index',compact('users'));
    }

    public function store(Request $request){
        $reponse= $request->except(['_token']);
        //dd($reponse);

        if($reponse['typecompte']=='1'){
            $valider = Validator::make($request->all(),[
                'compagny' =>'required|string',
                'email' =>'required|string|email',
                'pass' =>'max:255|min:6|required',
                'passconfirme' =>'max:255|min:6|required',
                'typecompte' =>'required',
            ]);
            $reponse['nom'] = $reponse['compagny'];
        }else{
            $valider = Validator::make($request->all(),[
                'nom' =>'max:255|min:3|required',
                'prenoms' =>'max:255|min:3|required',
                'sexe' =>'max:255|min:3|required',
                'email' =>'required|string|email',
                'pass' =>'max:255|min:6|required',
                'passconfirme' =>'max:255|min:6|required',
                'typecompte' =>'required',
            ]);
        }


        if($reponse['pass']!=$reponse['passconfirme']){
            return redirect()->route('user')->with('error','Les mots de passe doivent être identique');
        }
        //dd($reponse);
        if($valider->fails()){
            return redirect()->route('user')->withErrors($valider->errors());
        }else{
            try{
                $user = new User();
                $user->slug= str_slug(str_random(8));
                $user->name = $reponse['nom'];
                $user->prenoms = $reponse['prenoms'];
                $user->sexe = $reponse['sexe'];
                $user->email = $reponse['email'];
                $user->password = Hash::make($reponse['pass']);
                $user->typecompte = $reponse['typecompte'];
                $user->save();
                //dd($reponse);
                return redirect()->route('user')->with('success','✔ Félicitation ! l\'utilisateur a été ajouté');
            }catch (\Exception $e){
                //dd($e->getMessage());
            }

        }
    }

    public function storePigiste(Request $request){
        $reponse= $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'nom' =>'max:255|min:3|required',
            'prenoms' =>'max:255|min:3|required',
            'sexe' =>'max:255|min:3|required',
            'email' =>'required|string|email',
            'pass' =>'max:255|min:6|required',
            'passconfirme' =>'max:255|min:6|required',
        ]);

        if($reponse['pass']!=$reponse['passconfirme']){
            return redirect()->route('profiluser')->with('error','Les mots de passe doivent être identique');
        }
        //dd($reponse);
        if($valider->fails()){
            return redirect()->route('profiluser')->withErrors($valider->errors());
        }else{
            try{
                $user = new User();
                $user->slug= str_slug(str_random(8));
                $user->name = $reponse['nom'];
                $user->prenoms = $reponse['prenoms'];
                $user->sexe = $reponse['sexe'];
                $user->email = $reponse['email'];
                $user->password = Hash::make($reponse['pass']);
                $user->typecompte = '2';
                $user->etat = '0';
                //$user->useradd = Auth::user()->id;
                $user->save();
                //dd($reponse);
                return redirect()->route('profiluser')->with('success','✔ Félicitation ! l\'utilisateur a été ajouté');
            }catch (\Exception $e){
                //dd($e->getMessage());
            }

        }
    }

    public function edit($slug){
        $user=User::where('slug',$slug)->firstOrFail();
        return view('users/edit',compact('user'));
    }

    public function update($slug, Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);

        $valider = Validator::make($request->all(),[
            'nom' =>'max:255|min:3|required',
            'prenoms' =>'max:255|min:3|required',
            'sexe' =>'max:255|min:3|required',
            'typecompte' =>'required',
            'etat' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('user')->withErrors($valider->errors());
        }else{
            $user = User::where('slug',$slug)->firstOrFail();
            $user->name = $reponse['nom'];
            $user->prenoms = $reponse['prenoms'];
            $user->sexe = $reponse['sexe'];
            $user->typecompte = $reponse['typecompte'];
            $user->etat = $reponse['etat'];
            $user->save();
            //dd($reponse);
            return redirect()->route('user')->with('success','✔ Félicitation ! l\'utilisateur a été modifié');
        }

    }

    public function show($slug){
        $user=User::where('slug',$slug)->firstOrFail();
        $countgains = null;
        if($user->typecompte==2){
            $today = Carbon::today();
            $countgains = Gain::whereBetween('created_at', [$today->startOfMonth(), $today->endOfMonth()])->count();
        }
        return view('users/show',compact('user','countgains'));
    }
}
