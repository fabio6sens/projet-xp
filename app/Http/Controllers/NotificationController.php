<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use xp\Notification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $allnotif = DB::table('notifications')
            ->join('users','notifications.user_id', '=', 'users.id')
            ->join('annonceurs','notifications.annonceur_id', '=', 'annonceurs.id')
            ->join('pins','notifications.pin_id', '=', 'pins.slug')
            ->where('notifications.user_id',Auth::user()->id)
            ->orderBy('notifications.id','desc')
            ->select('pins.*','notifications.created_at AS dateM','notifications.read_at','notifications.id AS idnotif','notifications.type AS typenotif','notifications.data AS datanotif','annonceurs.*')
            ->take(50)
            ->get();
        //dd($allnotif);
        DB::table('notifications')->where([['user_id',Auth::user()->id],['read_at','0']])->update(['read_at' =>'1']);

        return view('notification/index',compact('allnotif'));
    }

    public function readAll(){
        DB::table('notifications')->where([['user_id',Auth::user()->id],['read_at','0']])->update(['read_at' =>'1']);
        return redirect()->route('alerts');
        //return json_encode(array('type'=>1));
    }

    public function indexclt()
    {
        $allnotif = DB::table('notifications')
            ->join('users','notifications.user_id', '=', 'users.id')
            ->join('annonceurs','notifications.annonceur_id', '=', 'annonceurs.id')
            ->join('pins','notifications.pin_id', '=', 'pins.slug')
            ->where('notifications.user_id',Auth::user()->id)
            ->orderBy('notifications.id','desc')
            ->select('pins.*','notifications.created_at AS dateM','notifications.read_at','notifications.id AS idnotif','annonceurs.*')
            ->take(50)
            ->get();
        //dd($allnotif);
        DB::table('notifications')->where([['user_id',Auth::user()->id],['read_at','0']])->update(['read_at' =>'1']);

        return view('pathmaster/notification/index',compact('allnotif'));
    }

    public function readAllclt(){
        DB::table('notifications')->where([['user_id',Auth::user()->id],['read_at','0']])->update(['read_at' =>'1']);
        return redirect()->route('alerts_path');
        //return json_encode(array('type'=>1));
    }
}
