<?php

namespace xp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use xp\Annonceur;

class AnnonceurController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $annonceurs = Annonceur::get();
        return view('annonceur/index',compact('annonceurs'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'etat' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('annonceur_path')->withErrors($valider->errors());
        }else{
            //if($request->hasFile('file')){

                $annonceur = new Annonceur();
                $annonceur->libelle = strtoupper($reponse['nom']);
                $annonceur->etat = $reponse['etat'];

                /*$file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '../public_html/piges/marqueurs';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);
                $annonceur->marqueur = $picture;*/

                $annonceur->save();

                return redirect()->route('annonceur_path')->with('success','✔ Félicitation ! l\'annonceur a été ajouté');

            /*}else{
                return redirect()->route('annonceur_path')->with('error','Veuillez inserez la Photo du marqueur');
            }*/
        }
        //dd($reponse);
    }

    public function itemAnnonce(Request $request)
    {
        $response=$request->all();
        $id=$response['id'];

        $annonceur=Annonceur::findOrFail($id);
        if($annonceur):
            $data[]=array (
                "id"  => $annonceur->id,
                "libelle"  => $annonceur->libelle,
                "etat"  => $annonceur->etat,
                //"marqueur"  => $annonceur->marqueur,
            );
            return json_encode(array('data'=>$data));
        else:
            return json_encode(array('data'=>0));
        endif;
    }

    public function update(Request $request){
        $reponse = $request->except(['_token']);
        //dd($request->file('file'));
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'etatUpdate' =>'required',
            'slug' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('annonceur_path')->withErrors($valider->errors());
        }else{

            $annonceur = Annonceur::findOrFail($reponse['slug']);
            $annonceur->libelle = $reponse['libelle'];
            $annonceur->etat = $reponse['etatUpdate'];

            /*if($request->hasFile('file')){
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '../public_html/piges/marqueurs';
                $picture = str_random(8).'.'. $extension;
                //dd($picture);
                if (!empty($annonceur->marqueur)) {
                    unlink($folderName.'/'.$annonceur->marqueur);
                }
                $annonceur->marqueur = $picture;
                $file->move($folderName,$picture);
            }*/

            $annonceur->save();

            return redirect()->route('annonceur_path')->with('success','✔ Félicitation ! l\'annonceur a été modifié');
        }
    }

    public function getPathAdd($id){
        //dd($id);
        return view('annonceur/add',compact('id'));
    }

    public function getAdd(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $id = $reponse['iddoublon'];

        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('annonce_get_path',$id)->withErrors($valider->errors());
        }else{

            $search = Annonceur::where('libelle',$reponse['nom'])->first();

            if($search == null){
                $annonceur = new Annonceur();
                $annonceur->libelle = strtoupper($reponse['nom']);
                $annonceur->save();

                if($id==0){
                    return redirect()->route('pins_add')->with('success','✔ Félicitation ! l\'annonceur a été ajouté');
                }else{
                    return redirect()->route('pins_double',$id);
                }
            }else{
                return redirect()->route('annonce_get_path',$id)->with('error','l\'annonceur existe déjà');
            }



        }
    }

}
