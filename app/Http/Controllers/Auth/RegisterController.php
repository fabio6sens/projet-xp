<?php

namespace xp\Http\Controllers\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use xp\User;
use xp\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'pass' => 'required|string|min:6',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        //dd($request->all());
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        //$this->guard()->login($user);
        $data=array(
            "token"=>$user->confirmation_token,
            "iduser"=>$user->slug,
            "nameUser"=>$user->name,
        );
        $email = $user->email;

        Mail::send('mail/validmail',$data,function ($message) use($email){
            $message->to($email);
            $message->subject('Veuillez confirmer votre inscription - ');

        });


        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('success','Veuillez terminer votre inscription en cliquant sur le lien que nous vous avons envoyé dans votre boîte mail');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \xp\User
     */
    protected function create(array $data)
    {
        $confirmation_token=str_replace('/','',bcrypt(str_random(10)));

        return User::create([
            'slug' => str_slug(str_random(8)),
            'name' => $data['nom'],
            'email' => $data['email'],
            'password' => bcrypt($data['pass']),
            'typecompte' => '2',
            'etat' => '0',
            'confirmation_token' => $confirmation_token,
        ]);
    }

    /**
     * Activer le compte de l'utilisateur.
     *
     * @return vue
     */
    public function activcompte($id, $token){
        $userExit = User::where('slug',$id)->where('confirmation_token',$token)->first();
        if($userExit){
            $userExit->update(['confirmation_token'=> null]);
            $userExit->update(['etat'=> '1']);
            //dd($userExit);
            $this->guard()->login($userExit);

            return redirect()->route('home')->with('success','Félicitation ! votre compte a bien été confirmé');
        }else{
            return redirect()->route('login')->with('error','Désolé ! ce lien ne semble plus valide');
        }
    }

}
