
var map;

$(document).ready(function(){
	//function initMap() {

		/*$('#pac-input').click(function(){
			alert($('#pac-input').val());
		});*/

		geoLocationInit();

	    function geoLocationInit(){
	    	if (navigator.geolocation) {
	    		navigator.geolocation.getCurrentPosition(success,fail);
	    	}else{
	    		alert("Le navigateur ne supporte pas")
	    	}
	    }

	    function success(position){
	    	lat = position.coords.latitude;
            lon = position.coords.longitude;

            var LatLng = new google.maps.LatLng(lat,lon);

            createmap(LatLng);

            // nearbySearch(LatLng,"school");
            allClients();
	    }

	    function fail(error){
	    	console.log(error);
	    	switch(error.code) {
                case error.PERMISSION_DENIED:
                    alert("Vous avez annuler la géolocalisation de cet document."); 
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("Information de géolocalisation indéfini. Vérifié votre connexion internet"); 
                    break;
                case error.TIMEOUT:
                    alert("La periode d'attente est dépassé. Vérifié votre connexion internet"); 
                    break;
                case error.UNKNOWN_ERROR:
                    alert("Erreur inconnue. Vérifié votre connexion internet"); 
                    break;
            }
	    }

	    function createmap(LatLng){
	    	map = new google.maps.Map(document.getElementById('map'), {
		      center: LatLng,
		      // scrollwheel:false,
		      zoom: 2
		    });

		    var marker = new google.maps.Marker({
			    position: LatLng,
			    map: map,
                animation: google.maps.Animation.DROP,
			    title: 'je suis ici'
		  	});

            boxsearch(map);

	    }

	    function createMarker(LatLng,icn,title,content){
            var infowindow = new google.maps.InfoWindow({
                content: content
            });

		    var marker = new google.maps.Marker({
			    position: LatLng,
			    map: map,
			    icon:icn,
                title: title,
				animation:  google.maps.Animation.DROP
			  });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
	    }

	    function allClients(){

	    	$.post('http://localhost:8080/6sens/learnmap/public/api/searchitem',function(match){
    			//console.log(match);
                match.forEach(function(value){
                    //console.log(value.bio);
                    var lat = value.latitude;
                    var lon = value.longitude;
                    var title = value.name;
                    var icon = '';

                    var LatLng = new google.maps.LatLng(lat,lon);
                    var contentString = '<a class="special" href="#"><span><b>' + value.bio + '</b></span></a>';

                    createMarker(LatLng,icon,title,contentString);
                });
	    	});

	    }

	    function boxsearch(map){
            var input = document.getElementById('pac-input');
            //alert(input);
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];

            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                console.log(places);
                //alert($('#pac-input').val());

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

		}

    	google.maps.event.addDomListener(window, 'load', geoLocationInit);

	  //   function nearbySearch(LatLng,type){
	  //   	var request = {
			//     location:  LatLng,
			//     radius: '50000',
			//     type: [type]
			// };

		 //    service = new google.maps.places.PlacesService(map);
			// service.nearbySearch(request, callback);

			// function callback(results, status) {
			// 	console.log(results);
			//   	if (status == google.maps.places.PlacesServiceStatus.OK) {
			// 	    for (var i = 0; i < results.length; i++) {
			// 	      var place = results[i];

			// 	      LatLng=place.geometry.location;
			// 	      icn=place.icon;
			// 	      title=place.name;

			// 	      createMarker(LatLng,icn,title);
			// 	    }
			//   	}
			// }

	  //   }	

	//}
});

