
var map,lastOpenedInfoWindow,latPosi,lonPosi;

var allMarkers=[];
//var markers=[];

// Map Zooming
var MapZoom = 12;

$(document).ready(function(){
	allClients();
	//google.maps.event.addDomListener(window, 'load', allClients);
});


	function allClients(){

        mycreatemap();

        // $.post('http://localhost:8080/6sens/projet-xp/public/api/searchitem',function(match){
        var allPins = Xp.allPins;
        //console.log(allPins);
        allPins.forEach(function(value){
            //console.log(value.bio);
            var lat = value.lat;
            var lon = value.lng;
            var title = value.annonceur_pins.libelle;
            var urltrue= $("#urlwebsite").val();
            var icon = urltrue+'/'+value.taille_pins.marqueur;

            var LatLng = new google.maps.LatLng(lat,lon);
            var date = new Date(value.updated_at),
                yr      = date.getFullYear(),
                month   = date.getMonth(),
                day     = date.getDate(),
                newDate = day+'-'+month+'-'+yr;

            var contentString = '<div style="width: 410px;">' +
                '<div class="">' +
                '<div class="d-flex flex-row">' +
                '<img src="piges/panneau/'+value.panneau+'" class="img-lgM rounded" alt="image panneau">' +
                '<div class="ml-3">' +
                '<p class="mt-2 text-success font-weight-bold">' + title + '</p>' +
                '<div class="cardMe-text">Marque : <b>' + value.marque + '</b> <br> Régie Pub : <b>' + value.regie + '</b> <br> Taille : <b>' + value.taille_pins.taille + '</b></div>' +
                '<div class="cardMe-footer"><small>Mise à jour le '+newDate+'</small><br><label class="badge badge-success" style="margin-top: 10px"><a href="' + value.profile_url +'" style="text-decoration: none;color: #FFF;"><i class="fa fa-eye"></i> Voir plus</a></label></div>' +
                '</div></div></div></div>';
            //var contentString = '<a class="special" href="#"><span><b>' + value.bio + '</b></span></a>';
            allMarkers.push(createMarker(LatLng,icon,title,contentString));
            //createMarker(LatLng,icon,title,contentString);
        });

        /*var clusterOptions = {
            gridSize: 60,
            minimumClusterSize: 2
        };
        var markerCluster = new MarkerClusterer(map, markers, clusterOptions);*/
        // });

    }

	function createMarker(LatLng,icn,title,content){
        /*var infowindow = new google.maps.InfoWindow({
            content: content
        });*/
        var infowindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: LatLng,
            map: map,
            icon:icn,
            title: title,
            animation:  google.maps.Animation.DROP
        });

        //markers.push(marker);

        marker.addListener('click', function() {
            closeLastOpenedInfoWindow();
            infowindow.setContent(content);
            infowindow.open(map, marker);
            lastOpenedInfoWindow = infowindow;
        });
    }

    function closeLastOpenedInfoWindow() {
        if (lastOpenedInfoWindow) {
            lastOpenedInfoWindow.close();
        }
    }

    /*function success(position){
        latPosi = position.coords.latitude;
        lonPosi = position.coords.longitude;
        console.log(latPosi);
        console.log(lonPosi);
    }*/

    /*function faild(error) {
        alert("erreur");
    }*/

	function mycreatemap(){
        /*if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success,faild);
        }else{
            alert("Le navigateur ne supporte pas")
        }*/

        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(5.2959048,-3.980355399999999),
            scrollwheel:true,
            zoom: MapZoom,
        });

	    /*var marker = new google.maps.Marker({
	 position: LatLng,
	 map: map,
	 animation: google.maps.Animation.DROP,
	 title: 'je suis ici'
	 });*/
    }




