(function($) {
  'use strict';
  $('.dropify').dropify({
      messages: {
          'default': 'Glissez-déposez un fichier ici ou cliquez',
          'replace': 'Glissez-déposez pour remplace ou cliquez',
          'remove':  'Retirer',
          'error':   'Oups, quelque chose s\'est mal passé.'
      }
  });
})(jQuery);