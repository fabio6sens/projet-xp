<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('etat', [
    'as' => 'etat',
    'uses' => 'EtatController@index',
]);

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index',
]);

Route::get('/confirm/{id}/{token}', [
    'as' => 'confirmUserCompte',
    'uses' => 'Auth\RegisterController@activcompte',
]);

Auth::routes();

//SEARCH PIN
Route::get('/SearchPinAd', [
    'as' => 'searchPinad',
    'uses' => 'HomeController@searchAd',
]);

Route::get('/SearchPin', [
    'as' => 'searchPin',
    'uses' => 'HomeController@searchForClt',
]);

Route::get('/searchPinmja', [
    'as' => 'searchPinmja',
    'uses' => 'HomeController@searchmja',
]);

Route::post('/searchmini', [
    'as' => 'searchmini',
    'uses' => 'HomeController@searchmini',
]);


Route::group(['middleware' => ['admins']], function(){
    Route::get('/home', 'HomeController@index')->name('home');

    //PAGE NOTIFICATION
    Route::get('/alert', [
        'as' => 'alerts',
        'uses' => 'NotificationController@index',
    ]);

    Route::get('/readallnotif', [
        'as' => 'readAllNotifs',
        'uses' => 'NotificationController@readAll',
    ]);
    /*Route::get('/readnotif', [
        'as' => 'readNotif',
        'uses' => 'NotificationController@readitem',
    ]);*/

    //PAGE PROFIL
    Route::get('/profil', [
        'as' => 'profiluser',
        'uses' => 'ProfilController@index',
    ]);

    Route::post('/profil', [
        'as' => 'updateuser',
        'uses' => 'ProfilController@update',
    ]);

    Route::post('/profilpass', [
        'as' => 'updateuserpass',
        'uses' => 'ProfilController@updatepass',
    ]);

    Route::post('/profilavatar', [
        'as' => 'updateuseravatar',
        'uses' => 'ProfilController@avatar',
    ]);

    //PAGE USERS
    Route::get('/utilisateurs', [
        'as' => 'user',
        'uses' => 'UserController@index',
    ]);

    Route::post('/utilisateurs', [
        'as' => 'user',
        'uses' => 'UserController@store',
    ]);

    Route::get('/utilisateurs/{slud}/edit', [
        'as' => 'edit-utilisateur',
        'uses' => 'UserController@edit',
    ]);

    Route::get('/utilisateurs/{slud}/show', [
        'as' => 'showutilisateur',
        'uses' => 'UserController@show',
    ]);

    Route::post('/utilisateurs/{slud}/update', [
        'as' => 'editutilisateur',
        'uses' => 'UserController@update',
    ]);

    Route::post('/utilisateurs-pigiste', [
        'as' => 'user-pigiste',
        'uses' => 'UserController@storePigiste',
    ]);

    //PAGE ANNONCEUR
    Route::get('/annonceur', [
        'as' => 'annonceur_path',
        'uses' => 'AnnonceurController@index',
    ]);

    Route::post('/annonceur', [
        'as' => 'annonce_store_path',
        'uses' => 'AnnonceurController@store',
    ]);

    Route::get('/annonceurget/{slug}', [
        'as' => 'annonce_get_path',
        'uses' => 'AnnonceurController@getPathAdd',
    ]);

    Route::post('/annonceurget', [
        'as' => 'annonce_get_path_store',
        'uses' => 'AnnonceurController@getAdd',
    ]);

    Route::get('/itemAnnonceur', [
        'as' => 'annonceur_info_path',
        'uses' => 'AnnonceurController@itemAnnonce',
    ]);

    Route::post('/annonceur_update', [
        'as' => 'annonce_update_path',
        'uses' => 'AnnonceurController@update',
    ]);

    //PAGE TAILLE
    Route::get('/size', [
        'as' => 'size_path',
        'uses' => 'TailleController@index',
    ]);

    Route::post('/size', [
        'as' => 'size_store_path',
        'uses' => 'TailleController@store',
    ]);

    Route::get('/itemSize', [
        'as' => 'size_info_path',
        'uses' => 'TailleController@itemsiza',
    ]);

    Route::post('/size_update', [
        'as' => 'size_update_path',
        'uses' => 'TailleController@update',
    ]);


    //PAGE PIGES
    Route::get('/{slug}/pin', [
        'as' => 'profil_path',
        'uses' => 'PigeController@index',
    ]);

    Route::get('/pins', [
        'as' => 'pins_path',
        'uses' => 'PigeController@all',
    ]);

    Route::get('/pins-valid', [
        'as' => 'pins_path_valid',
        'uses' => 'PigeController@allValid',
    ]);

    Route::get('/{slug}/valid-pin', [
        'as' => 'pins_valid',
        'uses' => 'PigeController@valid',
    ]);

    Route::post('/rejet-pin', [
        'as' => 'pins_rejet',
        'uses' => 'PigeController@rejet',
    ]);

    Route::get('/pins_add', [
        'as' => 'pins_add',
        'uses' => 'PigeController@add',
    ]);

    Route::post('/pins', [
        'as' => 'pins_store_path',
        'uses' => 'PigeController@store',
    ]);

    Route::get('/{slug}/edit-pins', [
        'as' => 'pins_edit_path',
        'uses' => 'PigeController@edit',
    ]);

    Route::post('/{slug}/edit-pins', [
        'as' => 'pins_update_path',
        'uses' => 'PigeController@update',
    ]);

    Route::get('/{slug}/add-pinsdouble', [
        'as' => 'pins_double',
        'uses' => 'PigeController@pinsdouble',
    ]);

    Route::post('/pinsdouble', [
        'as' => 'pinsdouble_store_path',
        'uses' => 'PigeController@storedouble',
    ]);



});


//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT
//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT
//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT
//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT//LISTE DES PAGES D'UN CLIENT

Route::group(['middleware' => ['annoncer']], function(){
    Route::get('/bienvenue', 'HomeController@indexclt')->name('homeclt');

    //PAGE EXPORTATION
    Route::get('/export', [
        'as' => 'export_path',
        'uses' => 'ExportController@index',
    ]);

    //PAGE NOTIFICATION
    Route::get('/alerts', [
        'as' => 'alerts_path',
        'uses' => 'NotificationController@indexclt',
    ]);

    Route::get('/readallnotifclt', [
        'as' => 'readAllNotifs_path',
        'uses' => 'NotificationController@readAllclt',
    ]);

    //PAGE PIGES
    Route::get('/{slug}/mypin', [
        'as' => 'pin_profil',
        'uses' => 'PigeController@indexclt',
    ]);

    //PAGE PROFIL
    Route::get('/monprofil', [
        'as' => 'profiluserclt',
        'uses' => 'ProfilCltController@index',
    ]);

    Route::post('/monprofil', [
        'as' => 'updateclt',
        'uses' => 'ProfilCltController@update',
    ]);

    Route::post('/monprofilpass', [
        'as' => 'updateuserpassclt',
        'uses' => 'ProfilCltController@updatepass',
    ]);

    Route::post('/monprofilavatar', [
        'as' => 'updateuseravatarclt',
        'uses' => 'ProfilCltController@avatar',
    ]);


    //PAGE CONFIGURATION
    Route::get('/myconfig', [
        'as' => 'config_path',
        'uses' => 'ConfigurationController@index',
    ]);

    Route::get('/myconfig/{id}/suivre/{libelle}', [
        'as' => 'del_path',
        'uses' => 'ConfigurationController@del',
    ]);

    Route::post('/myconfig/add', [
        'as' => 'add_path',
        'uses' => 'ConfigurationController@add',
    ]);

});