<?php

use Illuminate\Database\Seeder;
use xp\Pin;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i=0; $i < 100; $i++) {
            $clt = new Pin();
            $clt->slug = str_slug(str_random(8));
            $clt->lat = $faker->unique()->latitude(6,5);
            $clt->lng = $faker->unique()->longitude(-4,-5);
            $clt->type = $faker->lexify('Type ???');
            $clt->etat = $faker->lexify('Etat ???');
            $clt->user_id =  $faker->randomElement($array = array (1,3));
            $clt->annonceur_id = $faker->randomElement($array = array (1,2,3,4,5));
            $clt->note = $faker->randomElement($array = array ('1','2','3','4','5'));
            $clt->panneau = $faker->randomElement($array = array ('n6bRhf9F.png','g65rwbqN.jpg'));
            $clt->sectActiv = $faker->randomElement($array = array ('Industrie','production','fabrication','Télévision','Informelle','Artisanal'));
            $clt->marque = $faker->randomElement($array = array ('4G','Bock','Ivoire','Orange Banque','MoMo'));
            $clt->regie = $faker->randomElement($array = array ('SN Publistar','Canal Street','Indigo','PUB regie','Starcom'));
            $clt->agence = $faker->randomElement($array = array ('6sens','Voodou','MZgroup','OneStreet','Etoile'));
            $clt->taille_id = $faker->randomElement($array = array (1,2,3,4));
            $clt->save();
        }
    }

    /*public function run()
    {
        $faker = Faker\Factory::create('fr_FR');
        //$icon = array('','https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png','https://maps.google.com/mapfiles/kml/shapes/library_maps.png','https://maps.google.com/mapfiles/kml/shapes/info-i_maps.png','');
        $commune = array('Abobo','Adjamé','Yopougon','Le Plateau','Attécoubé','Cocody','Koumassi','Marcory','Port-Bouët','Treichville');
        $type = array('18 m&sup2','12 m&sup2','18 m&sup2','21 m&sup2','12 m&sup2');
        $icon = array('http://app.agence6sens.com/map/mapmoov.png','http://app.agence6sens.com/map/mapsolibra.png','http://app.agence6sens.com/map/mapbrassivoire.png','http://app.agence6sens.com/map/maporange.png','http://app.agence6sens.com/map/mapmtn.png');
        $nom = array('Moov','Solibra','Brassivoire','Orange Ci','Mtn');
        $marque = array('4G','Bock','Ivoire','Orange Banque','MoMo');
        $regie = array('SN Publistar','Canal Street','Indigo','PUB regie','Starcom');
        $j = 0;
        $e = 0;

        for ($i=0; $i < 100; $i++) {
            $clt = new Client();
            $clt->name = $nom[$j];
            $clt->latitude = $faker->unique()->latitude(6,5);
            $clt->longitude = $faker->unique()->longitude(-4,-5);
            $clt->bio = $faker->text;
            $clt->icone = $icon[$j];
            $clt->marque = $marque[$j];
            $clt->regie = $regie[$j];
            $clt->type = $type[$j];
            $clt->commune = $commune[$e];
            $clt->save();

            $e++;
            $j++;
            if($j==5){
                $j=0;
            }
            if($e==10){
                $e=0;
            }
        }
    }*/
}
